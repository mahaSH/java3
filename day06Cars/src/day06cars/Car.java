package day06cars;

/**
 *
 * @author 6155202
 */
class ValueInvalidException extends Exception {
    public ValueInvalidException(String msg) {
        super(msg);
    }
}
public class Car {
    enum FuelType{Gasoline,Diesel,Hypered,Electric,Other};
    private String makeMdel;
    private double engineSize;
    private FuelType fuelType;

    public Car(String makeMdel, double engineSize, FuelType fuelType) {
        setMakeMdel(makeMdel);
        setEngineSize(engineSize);
        setFuelType(fuelType);
        
    }
     public Car(String dataLine) throws ValueInvalidException {
        try {
            String[] data = dataLine.split(";");
            if (data.length != 3) {
                throw new IllegalArgumentException("Invalid number of items in data line");
            }
            setMakeMdel(data[0]); // ex
            setEngineSize(Double.parseDouble(data[1])); // ex NumberFormatException
           setFuelType(FuelType.valueOf(data[2])); // ex ParseException
        } catch (NumberFormatException  ex) {
            throw new IllegalArgumentException("Parsing error");
        }
    }

    @Override
    public String toString() {
        return String.format("%s has %.2f cylender,its fuel type is %s",makeMdel,engineSize,fuelType+"");
            }
    
    String toDataString() {
        return String.format("%s;%.2f;%s", makeMdel, engineSize, fuelType);
    }
    
    public String getMakeMdel() {
        return makeMdel;
    }

    public void setMakeMdel(String makeMdel) {
        this.makeMdel = makeMdel;
    }

    public double getEngineSize() {
        return engineSize;
    }

    public void setEngineSize(double engineSize) {
        this.engineSize = engineSize;
    }

    public FuelType getFuelType() {
        return fuelType;
    }

    public void setFuelType(FuelType fuelType) {
        this.fuelType = fuelType;
    }
    
    
}
