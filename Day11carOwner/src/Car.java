/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 6155196
 */
public class Car {
    
        private int id;
        private int ownerId;
        private String makeModel;
        private int prodYear;
        private String plate;
        private String ownerName; // computed, not stored in Car table

    public Car(int id, int ownerId, String makeModel, int prodYear, String plate,String ownerName) {
        this.id = id;
        this.ownerId = ownerId;
        this.makeModel = makeModel;
        this.prodYear = prodYear;
        this.plate = plate;
        this.ownerName = ownerName;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    public String getMakeModel() {
        return makeModel;
    }

    public void setMakeModel(String makeModel) {
        this.makeModel = makeModel;
    }

    public int getProdYear() {
        return prodYear;
    }

    public void setProdYear(int prodYear) {
        this.prodYear = prodYear;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    @Override
    public String toString() {
      String platePrint;
        if(plate==null){
            platePrint="with plate:"+plate;
        }else{
            platePrint="has no plate.";
        }
            
            
        return  String.format("%d: %s prod at %d %s.", id,makeModel,prodYear,platePrint);
                
          
    }
        
        
        
        
}
