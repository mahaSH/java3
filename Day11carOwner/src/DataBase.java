
import com.mysql.cj.jdbc.Blob;
import java.io.InvalidObjectException;
import javax.swing.DefaultListModel;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author 6155196
 */
public class DataBase {

    private Connection DBconnect = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;

    public DataBase() throws ClassNotFoundException, SQLException {

        Class.forName("com.mysql.cj.jdbc.Driver");
        // Setup the connection with the DB
        DBconnect = DriverManager
                .getConnection("jdbc:mysql://localhost/ipd20carowner?"
                        + "user=root&password=root");
    }

    static <T> void copyToModel(ArrayList<T> list, DefaultListModel<T> model) {
        model.clear();
        model.addAll(list);
    }

    ArrayList<Owner> getAllOwners() throws SQLException {
        ArrayList<Owner> list = new ArrayList<>();
        statement = DBconnect.createStatement();
        resultSet = statement.executeQuery(" SELECT o.name,  o.id, count(c.id) as totalhascars from owners o  left join cars c on   c.ownerId=o.id group by o.id;");
        while (resultSet.next()) {
//          Owner(int id, String name, byte[] photo, int carsOwned)
            int id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            int totalhascars = resultSet.getInt("totalhascars");

            Owner owner = new Owner(id, name, null, totalhascars);
            list.add(owner);
        }
        return list;
        // return owners and number of cars they own
        // SELECT *,COUNT(*) as carsOwned JOIN Owners onto Cars, GROUP BY ownerId

    }

    ArrayList<Car> getAllCars() throws SQLException {
        ArrayList<Car> list = new ArrayList<>();
        statement = DBconnect.createStatement();
        resultSet = statement.executeQuery("SELECT c.*,o.name as ownerName from owners o  right join cars c on   c.ownerId=o.id ;");
        while (resultSet.next()) {

            int id = resultSet.getInt("id");
            int ownerId = resultSet.getInt("ownerId");
            String makeModel = resultSet.getString("makeModel");
            int prodYear = resultSet.getInt("prodYear");
            String plate = resultSet.getString("plate");
            String ownerName = resultSet.getString("ownerName");

            Car car = new Car(id, ownerId, makeModel, prodYear, plate, ownerName);
            list.add(car);
        }
        return list;

        // return cars and possibly their owner names
        // SELECT * LEFT OUTER JOIN Cars onto Owner
    }

    ArrayList<Car> getAllCarsOwnedBy(int ownerId) throws SQLException {
        // return cars owned by a specific owner
        // SELECT * JOIN Cars onto Owner
        ArrayList<Car> list = new ArrayList<>();
        statement = DBconnect.createStatement();
        resultSet = statement.executeQuery(" SELECT c.id, c.makeModel,c.prodYear,plate from owners o left join cars c on   c.ownerId=o.id where o.id=" + ownerId);
        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String makeModel = resultSet.getString("makeModel");
            int prodYear = resultSet.getInt("prodYear");
            String plate = resultSet.getString("plate");

            Car car = new Car(id, ownerId, makeModel, prodYear, plate, null);
            list.add(car);
        }
        return list;

    }

    void updateOwner(Owner owner) throws UniqeValueRequiredException {

        try {
            String name = owner.getName();
            Blob photo = owner.getPhoto();
            int id = owner.getId();
            int hascas = owner.getCarsOwned();

            statement = DBconnect.createStatement();

            resultSet = statement.executeQuery("SELECT count(*) as number, name FROM owners  where name= '" + name + "'");
            resultSet.next();
            int num = resultSet.getInt("number");

            String nameDB = resultSet.getString("name");

            if (num == 0 || equals(num == 1 && nameDB.equals(name))) {// uniq
                preparedStatement = DBconnect.prepareStatement("update  owners set name=?, photo =? , carsOwned=? where id=?");
                preparedStatement.setString(1, name);
                preparedStatement.setBlob(2, photo);

                preparedStatement.setInt(3, hascas);
                preparedStatement.setInt(4, id);
                preparedStatement.executeUpdate();
            } else {//not uq
                System.out.println("name is used");
                throw new UniqeValueRequiredException("name is used");
            }

            // select first to check that owner name is not used yet
            // throw UniqeValueRequiredException if check fails
        } catch (SQLException ex) {
            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    void addOwner(Owner owner) throws SQLException, UniqeValueRequiredException {

        String name = owner.getName();
        Blob photo = owner.getPhoto();
        statement = DBconnect.createStatement();

        resultSet = statement.executeQuery("SELECT count(*) as number, name FROM owners where name= '" + name + "'");
        resultSet.next();
        int num = resultSet.getInt("number");

        String nameDB = resultSet.getString("name");

        if (num == 0 || equals(num == 1 && nameDB.equals(name))) {// uniq
            preparedStatement = DBconnect.prepareStatement("insert owners values(null,?,?,0);");
            preparedStatement.setString(1, name);
            preparedStatement.setBlob(2, photo);
            preparedStatement.executeUpdate();
        } else {//not uq
            System.out.println("name is used");
            throw new UniqeValueRequiredException("name is used");
        }

        // select first to check that owner name is not used yet
        // or that the only one with that name is owner.id
        // throw UniqeValueRequiredException if check fails
    }

    void updateCar(Car car) {

        // select first to check that plates are not used yet
        // throw UniqeValueRequiredException if check fails
    }

    void addCar(Car car) throws SQLException, UniqeValueRequiredException {

        String makeModel = car.getMakeModel();
        int ownerid = car.getOwnerId();
        String plate = car.getPlate();
        int year = car.getProdYear();

        statement = DBconnect.createStatement();

        resultSet = statement.executeQuery("SELECT count(*) as number, plate FROM cars where plate= '" + plate + "'");
        resultSet.next();
        int num = resultSet.getInt("number");

        String plateDB = resultSet.getString("plate");

        if (num == 0 || equals(num == 1 && plateDB.equals(plate))) {// uniq
            preparedStatement = DBconnect.prepareStatement("insert cars values(null,?,?,?,?);");
            preparedStatement.setInt(1, ownerid);
            preparedStatement.setString(2, makeModel);
            preparedStatement.setInt(3, year);
            preparedStatement.setString(4, plate);
            preparedStatement.executeUpdate();

        } else {//not uq
            throw new UniqeValueRequiredException("name is used");
        }
        // select first to check that plates are not used yet
        // or that the only one with those plates is car.id
        // throw UniqeValueRequiredException if check fails
    }

    void deleteCar(int id) throws SQLException {

        statement = DBconnect.createStatement();
        preparedStatement = DBconnect.prepareStatement("delete  from cars where id=?;");
        preparedStatement.setInt(1, id);
        preparedStatement.executeUpdate();
        
        System.out.println("delete done");

    }

    void deleteOwner(int id) throws SQLException {
        statement = DBconnect.createStatement();
        preparedStatement = DBconnect.prepareStatement("delete  from Owner where id=?;");
        preparedStatement.setInt(1, id);

        preparedStatement = DBconnect.prepareStatement("update cars  set Ownerid = '' where Ownerid=?;");
        preparedStatement.setInt(1, id);

        preparedStatement.executeUpdate();
        System.out.println("delete done");

        // consider database settings, one of:
        // RESTRICT | CASCADE | SET NULL (preferred in this case) | NO ACTION | SET DEFAULT
    }

    public class UniqeValueRequiredException extends Exception {

        public UniqeValueRequiredException(String name_is_used) {
        }

    }

}
