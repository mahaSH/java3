
import java.sql.Blob;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 6155196
 */
public class Owner {
    private int id;
    private String name;
    private Blob photo;
    private int carsOwned; // computed, not stored

    public Owner(int id, String name, Blob photo, int carsOwned) {
        this.id = id;
        this.name = name;
        this.photo = photo;
        this.carsOwned = carsOwned;
    }


    public Blob getPhoto() {
        return photo;
    }

    public void setPhoto(Blob photo) {
        this.photo = photo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCarsOwned() {
        return carsOwned;
    }

    public void setCarsOwned(int carsOwned) {
        this.carsOwned = carsOwned;
    }

    
    @Override
    public String toString() {//to Display In LstOwnerHasCars
        return String.format("%d:%s owns %d", id,name,carsOwned);
    }
    
}
