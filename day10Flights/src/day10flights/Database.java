package day10flights;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author 6155202
 */
public class Database {

    private Connection dbConn;

    Database() throws SQLException {
        try {// Setup the connection with the DB
            // Class.forName("com.mysql.jdbc.Driver");
            dbConn = DriverManager.getConnection("jdbc:mysql://localhost/day10flights?"
                    + "user=root&password=opel2003");
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println("database error");
            System.exit(1); // FATAL ERROR, EXIT PROGRAM
        }
    } // constructor will connect to database, initialize dbConn

    ArrayList<Flight> getAllFlights() throws SQLException {
        ArrayList<Flight> list = new ArrayList<Flight>();
        Statement stmtSelect = dbConn.createStatement();
        // Result set get the result of the SQL query
        ResultSet resultSet = stmtSelect.executeQuery("SELECT * FROM flights");
        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            Date on = resultSet.getDate("onday");
            String from = resultSet.getString("fromCode");
            String to = resultSet.getString("toCode");
            String type = resultSet.getString("type");
            int passengers = resultSet.getInt("passengers");
            Flight flight = new Flight(id, on, from, to, Flight.Type.valueOf(type), passengers);
            list.add(flight);
        }

        return list;
    }

    void addFlight(Flight flight) throws SQLException {

        PreparedStatement stmtInsert = dbConn
                .prepareStatement("INSERT INTO flights VALUES (NULL,?, ?, ?,?,?)");
        // Parameters start with 1
        java.sql.Date onDay = new java.sql.Date(flight.getOnDate().getTime());
        stmtInsert.setDate(1, onDay);
        stmtInsert.setString(2, flight.getFromCode());
        stmtInsert.setString(3, flight.getToCode());
        stmtInsert.setString(4, flight.getType() + "");
        stmtInsert.setInt(5, flight.getPassengers());
        stmtInsert.executeUpdate();

    }

    void updateFlight(Flight flight) throws SQLException {
        PreparedStatement stmtUpdate = dbConn
                .prepareStatement("UPDATE flights SET onDay=?, fromCode=?,toCode=?,type=?,passengers=? WHERE id=?");
        // Parameters start with 1
        java.sql.Date onDay = new java.sql.Date(flight.getOnDate().getTime());
        stmtUpdate.setDate(1, onDay);
        stmtUpdate.setString(2, flight.getFromCode());
        stmtUpdate.setString(3, flight.getToCode());
        stmtUpdate.setString(4, flight.getType() + "");
        stmtUpdate.setInt(5, flight.getPassengers());
        stmtUpdate.setInt(6, flight.getId());

        stmtUpdate.executeUpdate();
    }

    void deleteFlight(int id) throws SQLException {
        PreparedStatement stmtDelete = dbConn.prepareStatement("DELETE FROM flights WHERE id=?");
        System.out.println("delete");
        stmtDelete.setInt(1, id);
      stmtDelete.executeUpdate();
    }
    Flight selectFlightWhere(int id) throws SQLException {
        // Result set get the result of the SQL query
        String from = null;
        String to = null;
        int passengers = 0;
        Date date = null;
        Flight.Type type = null;
        Flight f = null;
        PreparedStatement stmtSelect = dbConn.prepareStatement("SELECT * FROM flights WHERE id=?");
        stmtSelect.setInt(1, id);
        ResultSet resultSet = stmtSelect.executeQuery();
        while (resultSet.next()) {
            int idenetity = resultSet.getInt("id");
            date = resultSet.getDate("onday");
            from = resultSet.getString("fromcode");
            to = resultSet.getString("tocode");
            type=Flight.Type.valueOf(resultSet.getString("type"));
           passengers = resultSet.getInt("passengers");
            f = new Flight(id, date, from,to,type, passengers);
        }
        return f; 
    }

}
