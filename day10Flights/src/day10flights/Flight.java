
package day10flights;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;

 

public class Flight {
   private int id;
   private Date onDate;
   private String fromCode ;//up to 15char
   private String toCode;//upto 15char
   private Type type;
   private int passengers;  
   enum Type{
    Domestic,Intenational,Private
}
static SimpleDateFormat dateFormat=new SimpleDateFormat("dd-MM-yyyy");
    public Flight(int id, Date onDate, String fromCode, String toCode, Type type, int passengers) {
        this.id = id;
        this.onDate = onDate;
        this.fromCode = fromCode;
        this.toCode = toCode;
        this.type = type;
        this.passengers = passengers;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getOnDate() {
        return onDate;
    }

    public void setOnDate(Date onDate) {
        this.onDate = onDate;
    }

    public String getFromCode() {
        return fromCode;
    }

    public void setFromCode(String fromCode) {
        if(fromCode.length()>5||fromCode.length()<3||!fromCode.matches("([a-zA-Z]+)([0-9#]+)")){
            System.out.println("from code should not be empty,Uppercase,with maximum 15 char");
            return;
        }
        this.fromCode = fromCode;
    }

    public String getToCode() {
        return toCode;
    }

    public void setToCode(String toCode) {
        if(toCode.length()>15||toCode.length()<0||!toCode.matches("([a-zA-Z]+)([0-9#]+)")){
            System.out.println("from code should not be empty,with maximum 15 char");
            return;
        }
        this.toCode = toCode;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public int getPassengers() {
        return passengers;
    }

    public void setPassengers(int passengers) {
        if(passengers>200){
            System.out.println("passenger number should be less then 200");
            return;
        }
        this.passengers = passengers;
    }

    @Override
    public String toString() {
        return String.format("%d:On %s From %s to %s,%s filght with%d passengers.",id,onDate,fromCode,toCode,type,passengers);
    }
    String toDataString(){
        return String.format("%d;%s;%s;%s;%s;%d",id,onDate,fromCode,toCode,type,passengers);
    }
}
