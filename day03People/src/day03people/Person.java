/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day03people;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author 6155202
 */
public class Person {

    String name;//1-100 characters excluding
    int heightCm;//0-250 cm
    Date dateOfBirth;//year must be 1900-2100 both inclusive

    public Person(String newName, int newHeight, Date DOB) {
        setName(newName);
        setHeight(newHeight);
        setDOB(DOB);
    }
    

    // Getter
    public String getName() {
        return name;
    }

    public int getHeight() {
        return heightCm;
    }

    public Date getDOB() {
        return dateOfBirth;

    }
    // Setter

    public void setName(String newName) {
        if (newName.length() < 2 || newName.length() > 50) {
            throw new IllegalArgumentException("Name must be between 2-50 characters long");
        }
        this.name = newName;
    }

    public void setHeight(int newHeight) {
        if (newHeight < 0 || newHeight > 250) {
            throw new IllegalArgumentException("height must be between 0-250");
        }
        this.heightCm = newHeight;
    }

    public void setDOB(Date DOB) {
        this.dateOfBirth = DOB;
    }

    @Override
    public String toString() {
        return String.format("%s is  %dcm tall born in %s", name, heightCm, dateFormat.format(getDOB()));
    }
    final static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

    //helper method
    public boolean matches(String search) {
        String name = getName();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd ");
        String heightStr = String.valueOf(getHeight());
        String dateStr = dateFormat.format(getDOB());
        if (name.contains(search) || dateStr.contains(search) || heightStr.contains(search)) {
            return (true);
        } else {
            return (false);
        }

    }
}
