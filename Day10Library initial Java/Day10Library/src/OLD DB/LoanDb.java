package day10library;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author AQ-37
 */
public class LoanDb {

    private Connection dbConn;

    LoanDb() throws SQLException {
        try {// Setup the connection with the DB
            // Class.forName("com.mysql.jdbc.Driver");
             /*dbConn = DriverManager.getConnection("jdbc:mysql://localhost/day10library?"
                    + "user=root&password=opel2003");*/
            dbConn = DriverManager.getConnection("jdbc:mysql://localhost/day10library?"
                    + "user=root&password=root");
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println("database error");
            System.exit(1); // FATAL ERROR, EXIT PROGRAM
        }
    }

    ArrayList<Loan> getAllLoans() throws SQLException {
        ArrayList<Loan> list = new ArrayList<Loan>();

        Statement stmtSelect = dbConn.createStatement();
        // Result set get the result of the SQL query
        ResultSet resultSet = stmtSelect.executeQuery("SELECT * FROM Loans");
        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            int memberId = resultSet.getInt("memberId");
            int bookId = resultSet.getInt("bookId");
            Date due = resultSet.getDate("dueDate");
            Date borrowed = resultSet.getDate("dateBorrowed");
            Date Returned = resultSet.getDate("dateReturned");

            Loan loan = new Loan(id, memberId, bookId, borrowed, due, Returned);
            list.add(loan);

        }
        return list;
    }

    void addLoan(Loan Loan) throws SQLException {

        PreparedStatement stmtInsert = dbConn
                .prepareStatement("INSERT INTO todos VALUES (NULL, ?, ?,?,?,?,?)");
        // Parameters start with 1
        stmtInsert.setInt(1, Loan.getMemberId());
        stmtInsert.setInt(2, Loan.getBookId());
        java.sql.Date dueDate = new java.sql.Date(Loan.getDueDate().getTime());
        java.sql.Date borrowed = new java.sql.Date(Loan.getDateBorrowed().getTime());
        java.sql.Date returned = new java.sql.Date(Loan.getDateReturned().getTime());
        stmtInsert.setDate(4, dueDate);
        stmtInsert.setDate(5, borrowed);
        stmtInsert.setDate(6, returned);
        stmtInsert.executeUpdate();
    }

    void updateLoan(Loan Loan) throws SQLException {
        PreparedStatement stmtUpdate = dbConn
                .prepareStatement("UPDATE todos SET task=?, difficulty=?,dueDate=?,status=? WHERE id=?");
        // Parameters start with 1
        stmtUpdate.setInt(1, Loan.getMemberId());
        stmtUpdate.setInt(2, Loan.getBookId());
        java.sql.Date dueDate = new java.sql.Date(Loan.getDueDate().getTime());
        java.sql.Date borrowed = new java.sql.Date(Loan.getDateBorrowed().getTime());
        java.sql.Date returned = new java.sql.Date(Loan.getDateReturned().getTime());
        stmtUpdate.setDate(4, dueDate);
        stmtUpdate.setDate(5, borrowed);
        stmtUpdate.setDate(6, returned);
        stmtUpdate.executeUpdate();
    }

    void deleteLoan(int id) throws SQLException {
        PreparedStatement stmtDelete = dbConn.prepareStatement("DELETE FROM Loans WHERE id=?");
        stmtDelete.setInt(1, id);
        stmtDelete.executeUpdate();
        //System.out.println(id);
    }

    Loan selectLoanWhere(int id) throws SQLException {
        // Result set get the result of the SQL query
       int memberid=0;
       int bookId=0;
       Date due = null;
       Date borrow = null;
       Date returned = null;
        Loan Loan = null;
        PreparedStatement stmtSelect = dbConn.prepareStatement("SELECT * FROM Loans WHERE id=?");
        stmtSelect.setInt(1, id);
        ResultSet resultSet = stmtSelect.executeQuery();
        while (resultSet.next()) {
            memberid= resultSet.getInt("memberId");
            bookId=resultSet.getInt("bookId");
            due = resultSet.getDate("dueDate");
            borrow = resultSet.getDate("dateBorrowed");
            returned = resultSet.getDate("dateReturned");
            Loan=  new Loan(id, memberid, bookId, borrow, due, returned);
        
    }
        return Loan;
    }
}
        
