package day10library;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author AQ-37
 */
public class BookDB {

    private Connection dbConn;

    BookDB() throws SQLException {
        try {// Setup the connection with the DB
            /*dbConn = DriverManager.getConnection("jdbc:mysql://localhost/day10library?"
                    + "user=root&password=opel2003");*/
            dbConn = DriverManager.getConnection("jdbc:mysql://localhost/day10library?"
                    + "user=root&password=root");
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println("database error");
            System.exit(1);
        }
    }

    ArrayList<Book> getAllBooks() throws SQLException {
        ArrayList<Book> list = new ArrayList<Book>();

        Statement stmtSelect = dbConn.createStatement();
        // Result set get the result of the SQL query
        ResultSet resultSet = stmtSelect.executeQuery("SELECT * FROM books");
        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String isbn = resultSet.getString("isbn");
            String authors = resultSet.getString("authors");
            String title = resultSet.getString("title");
            int copiesTotal = resultSet.getInt("copiesTotal");
            int rented = resultSet.getInt("currentCopiesRented");
            String genreStr = resultSet.getString("genre");
            Book book = new Book(id, isbn, authors, title, copiesTotal, rented, Book.Genre.valueOf(genreStr));
            list.add(book);

        }
        return list;
    }

    void addBook(Book Book) throws SQLException {

        PreparedStatement stmtInsert = dbConn
                .prepareStatement("INSERT INTO Books VALUES (NULL, ?, ?,?,?,?,?)");
        // Parameters start with 1
        stmtInsert.setString(1, Book.getIsbn());
        stmtInsert.setString(2, Book.getAuthors());
        stmtInsert.setString(3, Book.getTitle());
        stmtInsert.setInt(4, Book.getCopiesTotal());
        stmtInsert.setInt(5, Book.getCurrentCopiesRented());
        stmtInsert.setString(6, Book.getGenre() + "");
        stmtInsert.executeUpdate();
    }

    void updateBook(Book Book) throws SQLException {
        PreparedStatement stmtUpdate = dbConn
                .prepareStatement("UPDATE Books  SET isbn=?, authors=?,title=?,copiesTotal=?,currentCopiesRented=?,genre=? WHERE id=?");
        // Parameters start with 1
        stmtUpdate.setString(1, Book.getIsbn());
        stmtUpdate.setString(2, Book.getAuthors());
        stmtUpdate.setString(3, Book.getTitle());
        stmtUpdate.setInt(4, Book.getCopiesTotal());
        stmtUpdate.setInt(5, Book.getCurrentCopiesRented());
        stmtUpdate.setString(6, Book.getGenre() + "");
         stmtUpdate.setInt(7, Book.getId());
        stmtUpdate.executeUpdate();
    }

    void deleteBook(int id) throws SQLException {
        PreparedStatement stmtDelete = dbConn.prepareStatement("DELETE FROM books WHERE id=?");
        stmtDelete.setInt(1, id);
        stmtDelete.executeUpdate();
        //System.out.println(id);
    }

    Book selectBookWhere(int id) throws SQLException {
        // Result set get the result of the SQL query
        String isbn = null;
         String authors = null;
          String title = null;
        int total = 0;
        int rented = 0;
        Book.Genre genre = null;
        Book book = null;
        PreparedStatement stmtSelect = dbConn.prepareStatement("SELECT * FROM books WHERE id=?");
        stmtSelect.setInt(1, id);
        ResultSet resultSet = stmtSelect.executeQuery();
        while (resultSet.next()) {
          isbn = resultSet.getString("isbn");
           authors = resultSet.getString("authors");
           title = resultSet.getString("title");
            total= resultSet.getInt("copiesTotal");
            rented = resultSet.getInt("currentCopiesRented");
            genre = Book.Genre.valueOf(resultSet.getString("genre"));
            book = new Book(id, isbn,authors,title,total,rented,genre);
        }
        return book;
    }
}
