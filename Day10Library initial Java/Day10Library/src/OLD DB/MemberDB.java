/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day10library;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author AQ-37
 */
public class MemberDB {
  private Connection dbConn;

    MemberDB() throws SQLException {
        try {// Setup the connection with the DB
            // Class.forName("com.mysql.jdbc.Driver");
             /*dbConn = DriverManager.getConnection("jdbc:mysql://localhost/day10library?"
                    + "user=root&password=opel2003");*/
            dbConn = DriverManager.getConnection("jdbc:mysql://localhost/day10library?"
                    + "user=root&password=root");
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println("database error");
            System.exit(1); // FATAL ERROR, EXIT PROGRAM
        } 
    }

    ArrayList<Member> getAllMembers() throws SQLException {
        ArrayList<Member> list = new ArrayList<Member>();

        Statement stmtSelect = dbConn.createStatement();
        // Result set get the result of the SQL query
        ResultSet resultSet = stmtSelect.executeQuery("SELECT * FROM Members");
        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            String email = resultSet.getString("email");
            int max = resultSet.getInt("maxBooksAllowed");
            int current = resultSet.getInt("currentBooksRented");
           Member Member= new Member(id, name,email,max,current);
            list.add(Member);

        }
        return list;
    }

    void addMember(Member Member) throws SQLException {

        PreparedStatement stmtInsert = dbConn
                .prepareStatement("INSERT INTO Members VALUES (NULL, ?, ?,?,?,NULL)");
        // Parameters start with 1
        stmtInsert.setString(1, Member.getName());
        stmtInsert.setString(2, Member.getEmail());
        stmtInsert.setInt(3,Member.getMaxBooksAllowed());
        stmtInsert.setInt(4,Member.getCurrentBooksRented());
        stmtInsert.executeUpdate();
    }

    void updateMember(Member Member) throws SQLException {
        PreparedStatement stmtUpdate = dbConn
                .prepareStatement("UPDATE Members SET name=?, email=?,maxBooksAllowed=?,currentBooksRented=? WHERE id=?");
        // Parameters start with 1
        stmtUpdate.setString(1, Member.getName());
        stmtUpdate.setString(2, Member.getEmail());
        stmtUpdate.setInt(3,Member.getMaxBooksAllowed());
        stmtUpdate.setInt(4,Member.getMaxBooksAllowed());
         stmtUpdate.setInt(5,Member.getId());
        stmtUpdate.executeUpdate();
    }

    void deleteMember(int id) throws SQLException {
        PreparedStatement stmtDelete = dbConn.prepareStatement("DELETE FROM members WHERE id=?");
        stmtDelete.setInt(1, id);
        stmtDelete.executeUpdate();
        //System.out.println(id);
    }

    Member selectMemberWhere(int id) throws SQLException {
        // Result set get the result of the SQL query
        
        String name=null;
        String email=null;
        int max = 0;
        int current=0;
        Member Member = null;
        PreparedStatement stmtSelect = dbConn.prepareStatement("SELECT * FROM Members WHERE id=?");
        stmtSelect.setInt(1, id);
        ResultSet resultSet = stmtSelect.executeQuery();
        while (resultSet.next()) {
            name = resultSet.getString("name");
            email = resultSet.getString("email");
            max = resultSet.getInt("maxBooksAllowed");
          current = resultSet.getInt("currentBooksRented");
            
            Member = new Member(id,name,email,max,current);
        }
        return Member; 
    }   
}
