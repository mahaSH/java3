/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day10library;

import javax.swing.JOptionPane;

/**
 *
 * @author AQ-37
 */
public class Member {
private int id ;
private String name ;//VARCHAR(100)
private String email ;//VARCHAR(250)
private int maxBooksAllowed ;
private int currentBooksRented ;
//photo LARGEBLOB (bonus)
//Constructor
    public Member(int id, String name, String email, int maxBooksAllowed, int currentBooksRented) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.maxBooksAllowed = maxBooksAllowed;
        this.currentBooksRented = currentBooksRented;
    }
  //setters and getters 

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getMaxBooksAllowed() {
        return maxBooksAllowed;
    }

    public void setMaxBooksAllowed(int maxBooksAllowed) {
        this.maxBooksAllowed = maxBooksAllowed;
    }

    public int getCurrentBooksRented() {
        return currentBooksRented;
    }

    public void setCurrentBooksRented(int currentBooksRented) {
        this.currentBooksRented = currentBooksRented;
    }
    //OVERLOADED TOSTRING

    @Override
    public String toString() {
        return String.format("%d:%s(%s) max %d books",id,name,email,maxBooksAllowed); 
    }
    //FILTERING FUCTION  
    public boolean contains(String filter){
        String idStr=id+"";
        if(idStr.contains(filter))return true;  
        String maxStr=maxBooksAllowed+"";
        if (maxStr.contains(filter))return true;        
        String currentStr=currentBooksRented+"";
        if(currentStr.contains(filter))return true;
        if(name.contains(filter))return true;
        return false;
    }
}
