/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day10library;

import java.util.Date;

/**
 *
 * @author AQ-37
 */
public class Loan {
private int id ;
private int memberId;
private int bookId;
private Date dateBorrowed;
private Date dueDate ;
private Date dateReturned ;
//CONSTRUCTOR

    public Loan(int id, int memberId, int bookId, Date dateBorrowed, Date dueDate, Date dateReturned) {
        this.id = id;
        this.memberId = memberId;
        this.bookId = bookId;
        this.dateBorrowed = dateBorrowed;
        this.dueDate = dueDate;
        this.dateReturned = dateReturned;
    }
    //SETTERS AND GETTERS

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public Date getDateBorrowed() {
        return dateBorrowed;
    }

    public void setDateBorrowed(Date dateBorrowed) {
        this.dateBorrowed = dateBorrowed;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Date getDateReturned() {
        return dateReturned;
    }

    public void setDateReturned(Date dateReturned) {
        this.dateReturned = dateReturned;
    }
    //OVELOADDED TOSTRING

    @Override
    public String toString() {
        return String.format("%d:%d borrow %d in %s due date in %s returned in %s ",id,memberId,bookId,dateBorrowed,dueDate,dateReturned);
    }
   

  
}
