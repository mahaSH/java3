/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day10library;

/**
 *
 * @author AQ-37
 */
public class Book {
private int id;
private String isbn;// VARCHAR(13)
private String 	authors;// VARCHAR(200)
private String title;//VARCHAR(200)
private int copiesTotal;
private int currentCopiesRented;
private Genre genre;

   
 //CONSTRUCTOR

    public Book(int id, String isbn, String authors, String title, int copiesTotal, int currentCopiesRented, Genre genre) {
        setId(id);
        setIsbn(isbn);
        setAuthors(authors);
        setTitle(title);
        setCopiesTotal(copiesTotal);
        setCurrentCopiesRented(currentCopiesRented);
        setGenre(genre);
    }
    //setters and getters
     public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCopiesTotal() {
        return copiesTotal;
    }

    public void setCopiesTotal(int copiesTotal) {
        this.copiesTotal = copiesTotal;
    }

    public int getCurrentCopiesRented() {
        return currentCopiesRented;
    }

    public void setCurrentCopiesRented(int currentCopiesRented) {
        this.currentCopiesRented = currentCopiesRented;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }
enum Genre{
    Fiction, Novel,Mystery,Fantasy,Self_Help
}
//OVERLOADDED TOSTRING

    @Override
    public String toString() {
        return String.format("%d:%s \"%s\" by %s,%d copies in total,%d copies rented.%s book",id,isbn,title,authors,copiesTotal,currentCopiesRented,genre);
    }
    //FILTERING FUCTION  
    public boolean contains(String filter){
        String idStr=id+"";
        if(idStr.contains(filter))return true;
        if (isbn.contains(filter))return true;        
        if (authors.contains(filter))return true;  
        if (title.contains(filter))return true;  
        String copiesStr=currentCopiesRented+"";
        if(copiesStr.contains(filter))return true;
        String totalStr=copiesTotal+"";
        if(totalStr.contains(filter))return true;
        String genreStr=genre+"";
        if(genreStr.contains(filter))return true;
        return false;
    }
        
}
