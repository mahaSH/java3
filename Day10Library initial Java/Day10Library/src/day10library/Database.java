/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day10library;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author 6155202
 */
public class Database {
  private Connection dbConn;

    Database() throws SQLException {
        try {// Setup the connection with the DB
           dbConn = DriverManager.getConnection("jdbc:mysql://localhost/day10library?"
                    + "user=root&password=opel2003");
            /* dbConn = DriverManager.getConnection("jdbc:mysql://localhost/day10library?"
                    + "user=root&password=root");*/
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println("database error");
            System.exit(1);
        }
    }

    ArrayList<Book> getAllBooks() throws SQLException {
        ArrayList<Book> list = new ArrayList<Book>();

        Statement stmtSelect = dbConn.createStatement();
        // Result set get the result of the SQL query
        ResultSet resultSet = stmtSelect.executeQuery("SELECT * FROM books");
        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String isbn = resultSet.getString("isbn");
            String authors = resultSet.getString("authors");
            String title = resultSet.getString("title");
            int copiesTotal = resultSet.getInt("copiesTotal");
            int rented = resultSet.getInt("currentCopiesRented");
            String genreStr = resultSet.getString("genre");
            Book book = new Book(id, isbn, authors, title, copiesTotal, rented, Book.Genre.valueOf(genreStr));
            list.add(book);

        }
        return list;
    }

    void addBook(Book Book) throws SQLException {

        PreparedStatement stmtInsert = dbConn
                .prepareStatement("INSERT INTO Books VALUES (NULL, ?, ?,?,?,?,?)");
        // Parameters start with 1
        stmtInsert.setString(1, Book.getIsbn());
        stmtInsert.setString(2, Book.getAuthors());
        stmtInsert.setString(3, Book.getTitle());
        stmtInsert.setInt(4, Book.getCopiesTotal());
        stmtInsert.setInt(5, Book.getCurrentCopiesRented());
        stmtInsert.setString(6, Book.getGenre() + "");
        stmtInsert.executeUpdate();
    }

    void updateBook(Book Book) throws SQLException {
        PreparedStatement stmtUpdate = dbConn
                .prepareStatement("UPDATE Books  SET isbn=?, authors=?,title=?,copiesTotal=?,currentCopiesRented=?,genre=? WHERE id=?");
        // Parameters start with 1
        stmtUpdate.setString(1, Book.getIsbn());
        stmtUpdate.setString(2, Book.getAuthors());
        stmtUpdate.setString(3, Book.getTitle());
        stmtUpdate.setInt(4, Book.getCopiesTotal());
        stmtUpdate.setInt(5, Book.getCurrentCopiesRented());
        stmtUpdate.setString(6, Book.getGenre() + "");
         stmtUpdate.setInt(7, Book.getId());
        stmtUpdate.executeUpdate();
    }

    void deleteBook(int id) throws SQLException {
        PreparedStatement stmtDelete = dbConn.prepareStatement("DELETE FROM books WHERE id=?");
        stmtDelete.setInt(1, id);
        stmtDelete.executeUpdate();
        //System.out.println(id);
    }

    Book selectBookWhere(int id) throws SQLException {
        // Result set get the result of the SQL query
        String isbn = null;
         String authors = null;
          String title = null;
        int total = 0;
        int rented = 0;
        Book.Genre genre = null;
        Book book = null;
        PreparedStatement stmtSelect = dbConn.prepareStatement("SELECT * FROM books WHERE id=?");
        stmtSelect.setInt(1, id);
        ResultSet resultSet = stmtSelect.executeQuery();
        while (resultSet.next()) {
          isbn = resultSet.getString("isbn");
           authors = resultSet.getString("authors");
           title = resultSet.getString("title");
            total= resultSet.getInt("copiesTotal");
            rented = resultSet.getInt("currentCopiesRented");
            genre = Book.Genre.valueOf(resultSet.getString("genre"));
            book = new Book(id, isbn,authors,title,total,rented,genre);
        }
        return book;
    }
    //LOAN DATABASE
    ArrayList<Loan> getAllLoans() throws SQLException {
        ArrayList<Loan> list = new ArrayList<Loan>();

        Statement stmtSelect = dbConn.createStatement();
        // Result set get the result of the SQL query
        ResultSet resultSet = stmtSelect.executeQuery("SELECT * FROM Loans");
        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            int memberId = resultSet.getInt("memberId");
            int bookId = resultSet.getInt("bookId");
            Date due = resultSet.getDate("dueDate");
            Date borrowed = resultSet.getDate("dateBorrowed");
            Date Returned = resultSet.getDate("dateReturned");

            Loan loan = new Loan(id, memberId, bookId, borrowed, due, Returned);
            list.add(loan);

        }
        return list;
    }

    void addLoan(Loan Loan) throws SQLException {

        PreparedStatement stmtInsert = dbConn
                .prepareStatement("INSERT INTO todos VALUES (NULL, ?, ?,?,?,?,?)");
        // Parameters start with 1
        stmtInsert.setInt(1, Loan.getMemberId());
        stmtInsert.setInt(2, Loan.getBookId());
        java.sql.Date dueDate = new java.sql.Date(Loan.getDueDate().getTime());
        java.sql.Date borrowed = new java.sql.Date(Loan.getDateBorrowed().getTime());
        java.sql.Date returned = new java.sql.Date(Loan.getDateReturned().getTime());
        stmtInsert.setDate(4, dueDate);
        stmtInsert.setDate(5, borrowed);
        stmtInsert.setDate(6, returned);
        stmtInsert.executeUpdate();
    }

    void updateLoan(Loan Loan) throws SQLException {
        PreparedStatement stmtUpdate = dbConn
                .prepareStatement("UPDATE todos SET task=?, difficulty=?,dueDate=?,status=? WHERE id=?");
        // Parameters start with 1
        stmtUpdate.setInt(1, Loan.getMemberId());
        stmtUpdate.setInt(2, Loan.getBookId());
        java.sql.Date dueDate = new java.sql.Date(Loan.getDueDate().getTime());
        java.sql.Date borrowed = new java.sql.Date(Loan.getDateBorrowed().getTime());
        java.sql.Date returned = new java.sql.Date(Loan.getDateReturned().getTime());
        stmtUpdate.setDate(4, dueDate);
        stmtUpdate.setDate(5, borrowed);
        stmtUpdate.setDate(6, returned);
        stmtUpdate.executeUpdate();
    }

    void deleteLoan(int id) throws SQLException {
        PreparedStatement stmtDelete = dbConn.prepareStatement("DELETE FROM Loans WHERE id=?");
        stmtDelete.setInt(1, id);
        stmtDelete.executeUpdate();
        //System.out.println(id);
    }

    Loan selectLoanWhere(int id) throws SQLException {
        // Result set get the result of the SQL query
       int memberid=0;
       int bookId=0;
       Date due = null;
       Date borrow = null;
       Date returned = null;
        Loan Loan = null;
        PreparedStatement stmtSelect = dbConn.prepareStatement("SELECT * FROM Loans WHERE id=?");
        stmtSelect.setInt(1, id);
        ResultSet resultSet = stmtSelect.executeQuery();
        while (resultSet.next()) {
            memberid= resultSet.getInt("memberId");
            bookId=resultSet.getInt("bookId");
            due = resultSet.getDate("dueDate");
            borrow = resultSet.getDate("dateBorrowed");
            returned = resultSet.getDate("dateReturned");
            Loan=  new Loan(id, memberid, bookId, borrow, due, returned);
        
    }
        return Loan;
    }
    //MEMBER DATABASE
    ArrayList<Member> getAllMembers() throws SQLException {
        ArrayList<Member> list = new ArrayList<Member>();

        Statement stmtSelect = dbConn.createStatement();
        // Result set get the result of the SQL query
        ResultSet resultSet = stmtSelect.executeQuery("SELECT * FROM Members");
        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            String email = resultSet.getString("email");
            int max = resultSet.getInt("maxBooksAllowed");
            int current = resultSet.getInt("currentBooksRented");
           Member Member= new Member(id, name,email,max,current);
            list.add(Member);

        }
        return list;
    }

    void addMember(Member Member) throws SQLException {

        PreparedStatement stmtInsert = dbConn
                .prepareStatement("INSERT INTO Members VALUES (NULL, ?, ?,?,?,NULL)");
        // Parameters start with 1
        stmtInsert.setString(1, Member.getName());
        stmtInsert.setString(2, Member.getEmail());
        stmtInsert.setInt(3,Member.getMaxBooksAllowed());
        stmtInsert.setInt(4,Member.getCurrentBooksRented());
        stmtInsert.executeUpdate();
    }

    void updateMember(Member Member) throws SQLException {
        PreparedStatement stmtUpdate = dbConn
                .prepareStatement("UPDATE Members SET name=?, email=?,maxBooksAllowed=?,currentBooksRented=? WHERE id=?");
        // Parameters start with 1
        stmtUpdate.setString(1, Member.getName());
        stmtUpdate.setString(2, Member.getEmail());
        stmtUpdate.setInt(3,Member.getMaxBooksAllowed());
        stmtUpdate.setInt(4,Member.getMaxBooksAllowed());
         stmtUpdate.setInt(5,Member.getId());
        stmtUpdate.executeUpdate();
    }

    void deleteMember(int id) throws SQLException {
        PreparedStatement stmtDelete = dbConn.prepareStatement("DELETE FROM members WHERE id=?");
        stmtDelete.setInt(1, id);
        stmtDelete.executeUpdate();
        //System.out.println(id);
    }

    Member selectMemberWhere(int id) throws SQLException {
        // Result set get the result of the SQL query
        
        String name=null;
        String email=null;
        int max = 0;
        int current=0;
        Member Member = null;
        PreparedStatement stmtSelect = dbConn.prepareStatement("SELECT * FROM Members WHERE id=?");
        stmtSelect.setInt(1, id);
        ResultSet resultSet = stmtSelect.executeQuery();
        while (resultSet.next()) {
            name = resultSet.getString("name");
            email = resultSet.getString("email");
            max = resultSet.getInt("maxBooksAllowed");
          current = resultSet.getInt("currentBooksRented");
            
            Member = new Member(id,name,email,max,current);
        }
        return Member; 
    }   
}
