/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz1todos;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author 6155202
 */
class InvalidValueException extends Exception {
	InvalidValueException(String msg) { super(msg); }
}
 enum Status { Pending, Done, Delegated }

public class Todo {
	private String task; // 1-100 characters, made up of uppercase and lowercase letters, digits, space, _-*?%#@(),./\ only
	private int difficulty; // 1-5, as slider
	private Date dueDate; // year 1900-2100 both inclusive, use formatted field
	private Status status; // enum, combo box
        final static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

    public Todo(String task, int difficulty, Date dueDate, Status status) {
        setTask(task);
        setDifficulty(difficulty);
        setDueDate(dueDate) ;
        setStatus(status);
    }

    @Override
    public String toString() {
       return String.format("%s by %s/diificulty %d,%s",
              task,dateFormat.format(dueDate),difficulty,status.toString());
    }
    String toDataString() {
        return String.format("%s;%d;%s,%s", task, difficulty, dateFormat.format(dueDate),status.toString());
    }

    public Todo(String dataLine) {
        try {
            String[] data = dataLine.split(";");
            if (data.length != 4) {
                throw new IllegalArgumentException("Invalid number of items in data line");
            }
            setTask(data[0]); // ex
            setDifficulty(Integer.parseInt(data[1])); // ex NumberFormatException
            setDueDate(dateFormat.parse(data[2])) ;// ex ParseException
            setStatus(Status.valueOf("data[3]"));
        } catch (NumberFormatException | ParseException ex) {
            throw new IllegalArgumentException("Parsing error");
        }
    }

    

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        if(task.isEmpty()||task.length()<1||task.length()>100||!task.matches("[A-Za-z.\\s_?%#@()-/]+")){
            throw new IllegalArgumentException("Name must be 1-100 characters long and not contain semicolons");
        }
        this.task = task;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        if(difficulty<1||difficulty>5){
             throw new IllegalArgumentException("difficulty should be between 1 and 5");
        }
        this.difficulty = difficulty;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(dueDate);
        int year = cal.get(Calendar.YEAR);
        if (year < 1900 | year > 2100) {
            throw new IllegalArgumentException("Year must be in 1900-2100 range");
        }
        this.dueDate = dueDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
        
}

