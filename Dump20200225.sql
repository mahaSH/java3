-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: final
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cards`
--

DROP TABLE IF EXISTS `cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permcode` varchar(14) NOT NULL,
  `photo` longblob NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cards`
--

LOCK TABLES `cards` WRITE;
/*!40000 ALTER TABLE `cards` DISABLE KEYS */;
INSERT INTO `cards` VALUES (1,'ASDF1234567890',_binary '�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0\0\0\0\0\0\0��h6\0\0\0sRGB\0�\�\�\0\0\0gAMA\0\0���a\0\0\0	pHYs\0\0\�\0\0\�\�o�d\0\0\0IDAT8OcP\�\�Cb I5P�bBl4�G(\0j\�@���\0\0\0\0IEND�B`�'),(2,'SDFG1234561234',_binary '�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0d\0\0\0d\0\0\0p\�T\0\0\rIDATx^\�YpT\��#L����$�]\�Tl0�\r6\�¦�\�8��<�\�\�\�\�)�`0��R�T�TⰘ}\�*Vo �ľ#$�F\�%�т\�4�\���i\���#͌n��B<|�蹷��\�\�\�N�x<��x��%&\�\�\r�7^�fN��\�\���}���\�����\�+/���e\�\�xM����\�p\r�\�=�y /䉼\��\\Kp��}�\�\�OӌiЖ\r�)?\�,\�n���ݮ��\��I�o\�\�D\�}\�yy�\�\�\��_\�2P\�T\�p�`�^�\�	/\�\�/�CNeҭ�r)�*�ݠ��2\���&����E�\�X��?�S�\�\�(\��q�]_�3\rl�-�	���\Z�%h#!!�F�\ZA\�׬�[B�x^A��M�m\��e4z\�pi�ZͰ\�\�ٳ\'�}v4m\\�R6��n�nLYIc�%\�\�K,�V8��/��\��\nw`;ꀺ��\�\0K���\�C�Ź��]\�\�\�\�\�i3,�S�\�у�?=��Hwe\�YP�\��vѰ���uU\�o,!nz�\�%�A5\���\"�\Z\�e^zd�U:	K����~�j�B\nܨf\�߫����-��~�L�N�bf\�����\Z\�]\�ܟF�?\�t��\�C\�\�,fdw�g\�\���5/OO���̸\�\n��&�N1\��\��\�&Qu��\�݁&\�F\�+XB�\�[\�3Н\��s@�N<),�]\�f�}M�^[c%j)Pq��E�ɟ�AMW���\�\�t)�.��\�\�\�M\��@�\nT_�\�2j6\���Fq�),!\"\�Mk�\�!��p����cDܓ���{(Py�����4��>�\�K�\�#]\�UB�\�\��\�B\�\�\"�s�k��)\�ek\0]\�\�),��\�(}jav��J��4!5 ����l2\�\�`�È�%���\ZL�\����D����T��O�j��@;L�D9�\�\��D�\�)4\�\�q�Sh�T�줺\�+\'$U}-`	a\�n7\�rZzK\\(�����\'�YbU_XB�gh�B\�Ѧ�\�L�T�\�6\���b=�%H�:�{q\�_���\�(\�)\�,�\�4\��Aջ]�`�K�jfv�\�u�݈��\�f�\�,�?��5z� t��\��� �#\�3t�U{\�\�\"pB\�\�\�!{At����\�K\\\�v��KO�\��	�Y\"��\' nJw��s\�5\rb\�4\�ЫA\��*\Z1|�\�\���7ۍ�8��\�*��Qs�\�W6HY�TuF�CB��=�F[}}�n.��H�@�en�\� �\�\"l�\�תu\�\���]\"��T9�\�l�h\r\�-�\�o�7َx77\�~k!��H%\�in���u�[�D�7V��o�\"��Ԗ����6ͱ�9�3ԋ�\�X)�\�Bw\�/9\�m\����9���qE�P]\�!\�\�r�tf۝\\��-�z���CJ��-@{� \�\�3��L�6\n\"�	�̫ڮ	h?�\�A�`G*6A�iC�����ۉh\�\�\���Z�\�\�`l�f�ah1�n7f\�!m�\�`��\�\�.�^�10,\�k!��Hm	�Pm\�|\0_x��\�X�\��K�\�\"\�3�	\�T�r�}�D�.7\��bM����٭�\0��\�\�	\���\�\�\�rS�t����灨\�&\�\�\�\"��ܝ�{�ڭ�CZ\�t�\�\�2!\\:�\�&�/<V?\�F\�W�aB��\�<�\�\�8�/<8\�H�@;X�f0!\�B�ʁ/�\0��\��)���\�\�*�+\�\�B�Z���\�5%\�\�\�	|�n g��8\�H`p\�\�\�	\�re��7�\�j\nD�xn8�ʪ	.\�p\�0�b�A7|�\�1�_�->F/\"\�䊅�f��zQ�\�YͶ`\�\�Y�j?\0|\�\�>w�}`\n9@t\�J��;��r`��\�6��9|�jG�e\�V	��S\'K\�f�\�_r�	\���\�;b��;3�\�\�Ta��D�u{��\�\'�\�5\�Ta \����&�L��\�ϐ��^\�\���HƐ�t�\��	-\��\�(��V/0I��\�ѝ��Ɨg�X\�f\n�@9`�۶M)\�\�87H\�b<��\n�u\��#����mGnbh�3s[��r�1a�r�Gjkߨ\�I�R�\��\�4,�4�o_\�:s�]h���O	\�\�b68\0\�Â���y��#t\0NSP�\��\�\�\�uv \�\n����!䐉/�w����eTwj-\�N\�d�R\�ᕲ,�|\�`\�6K1�`[Un\�)v�i\reT���\�N�0!\�\�N\�f�<�L�\�dYj�����\r�i\�\'�N�\0}9\�^B\�]�S\�0A;��\�C+(o\�<W8ZC�>s��m\��^A�G\�H�@�k�+\��^7Vn�\�H%��\�<AɁdY�Z�I�u�ۢ���dv�I R\�\�\rT�oYH�\�K\����L\����k�0cQ(��\�;�\�q�@kU\�Gk�G�C|\�ք}��Ĕ\�_�c\ZN�P�\�\�\�`� }�x\�VQ�\�A@\�\�ᡱGd�\�@�\�u�X�����\��Z\�\�)�\�^DU\�U\�(�s\�\�&�J��N���vA�Bv�Q}\�\�`ޢ���o\nh\�\�3�\��L��]�C���C\��S~Z�MQ��sU��@k�cE�xRB\�\0����oh�U\�#:$x�\�<��n�UTwv[�3Z�ㆫ�\�\�G�\\A5y�,��ڡ\�f`У�J�mA�!k\'�-��PrX	\�戎A�򴩠L�\�_�\������M��}\\\�/�\�\�㲑U\�j�*\�0��FOM~\�	ڍ\�#�N\�\�mAٰ���\�I��l\�\�\�Ģg\�T\���ŧ\����B�uL ��-�	��F�{a�>&\� \���,�\�\�M5t��5\�즺3�[��l��m�6\�v\�!��\��Qj\�A\���\�\�Q\�>y�\�\��\�xy�No\�\�vYDjE]P\'\�-�z�\�9\�\Z\�r\�8�A��\�3\�ݮ�D�yE\�e+\�ޓ�:���3\�.a\�?d`\�a�����\n]	ݩ̦Fo&\�_�*�t\'Du\Z@hm���?W!�S�l���\��ڸOd��ꠋkKh��J\���3\�4�V�~Q�:\�FR\�}�D\�L\Z��E	K��7\'>w\�)@�\�/�ez\�\0K�\Z<)�__w��x2Za	1���7}13��\r\�l3TXB\�<�X:���\�\�\�\��5�ѐA1�<^$XB\\�(�/�|�13�^\'�S)�\�\�L�N�\�w\�\��\�N\�\�\nꈺ�~0\�x���N���\�C�����N�\�yE�:�\�6�!\�\�M\�\nK�\�\�L{\�\r*ؽ�U����|��_RBB\�\�>;`	��\�n�gP\��dV��lGll�ۃ%\�N/�\Z7\�\�~\�t˵m�R}t����j�4���ֱ\�F\�̠�\�6 .���=�\�[��[�#\Z���\�йs\�Z\����\r\�ϕ6\r\��cf�AX�Q~���\�FѢ�N�\�[gIaj��0�\�e��S[fɲa\�C\�\�\�\�s\0�\�?L\�C\�L3\��5m�;���[(�\�\�lp�@^\�y��\�/1�s\�-�W�г\'%%>LOHoN|�f�=��\�]JK��\�>��K�\��*&<\�������\"\�<��Z�[�?�S����7�\0\0\0\0IEND�B`�'),(3,'ASDF1234523475',_binary '�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0d\0\0\0d\0\0\0p\�T\0\0\rIDATx^\�YpT\��#L����$�]\�Tl0�\r6\�¦�\�8��<�\�\�\�\�)�`0��R�T�TⰘ}\�*Vo �ľ#$�F\�%�т\�4�\���i\���#͌n��B<|�蹷��\�\�\�N�x<��x��%&\�\�\r�7^�fN��\�\���}���\�����\�+/���e\�\�xM����\�p\r�\�=�y /䉼\��\\Kp��}�\�\�OӌiЖ\r�)?\�,\�n���ݮ��\��I�o\�\�D\�}\�yy�\�\�\��_\�2P\�T\�p�`�^�\�	/\�\�/�CNeҭ�r)�*�ݠ��2\���&����E�\�X��?�S�\�\�(\��q�]_�3\rl�-�	���\Z�%h#!!�F�\ZA\�׬�[B�x^A��M�m\��e4z\�pi�ZͰ\�\�ٳ\'�}v4m\\�R6��n�nLYIc�%\�\�K,�V8��/��\��\nw`;ꀺ��\�\0K���\�C�Ź��]\�\�\�\�\�i3,�S�\�у�?=��Hwe\�YP�\��vѰ���uU\�o,!nz�\�%�A5\���\"�\Z\�e^zd�U:	K����~�j�B\nܨf\�߫����-��~�L�N�bf\�����\Z\�]\�ܟF�?\�t��\�C\�\�,fdw�g\�\���5/OO���̸\�\n��&�N1\��\��\�&Qu��\�݁&\�F\�+XB�\�[\�3Н\��s@�N<),�]\�f�}M�^[c%j)Pq��E�ɟ�AMW���\�\�t)�.��\�\�\�M\��@�\nT_�\�2j6\���Fq�),!\"\�Mk�\�!��p����cDܓ���{(Py�����4��>�\�K�\�#]\�UB�\�\��\�B\�\�\"�s�k��)\�ek\0]\�\�),��\�(}jav��J��4!5 ����l2\�\�`�È�%���\ZL�\����D����T��O�j��@;L�D9�\�\��D�\�)4\�\�q�Sh�T�줺\�+\'$U}-`	a\�n7\�rZzK\\(�����\'�YbU_XB�gh�B\�Ѧ�\�L�T�\�6\���b=�%H�:�{q\�_���\�(\�)\�,�\�4\��Aջ]�`�K�jfv�\�u�݈��\�f�\�,�?��5z� t��\��� �#\�3t�U{\�\�\"pB\�\�\�!{At����\�K\\\�v��KO�\��	�Y\"��\' nJw��s\�5\rb\�4\�ЫA\��*\Z1|�\�\���7ۍ�8��\�*��Qs�\�W6HY�TuF�CB��=�F[}}�n.��H�@�en�\� �\�\"l�\�תu\�\���]\"��T9�\�l�h\r\�-�\�o�7َx77\�~k!��H%\�in���u�[�D�7V��o�\"��Ԗ����6ͱ�9�3ԋ�\�X)�\�Bw\�/9\�m\����9���qE�P]\�!\�\�r�tf۝\\��-�z���CJ��-@{� \�\�3��L�6\n\"�	�̫ڮ	h?�\�A�`G*6A�iC�����ۉh\�\�\���Z�\�\�`l�f�ah1�n7f\�!m�\�`��\�\�.�^�10,\�k!��Hm	�Pm\�|\0_x��\�X�\��K�\�\"\�3�	\�T�r�}�D�.7\��bM����٭�\0��\�\�	\���\�\�\�rS�t����灨\�&\�\�\�\"��ܝ�{�ڭ�CZ\�t�\�\�2!\\:�\�&�/<V?\�F\�W�aB��\�<�\�\�8�/<8\�H�@;X�f0!\�B�ʁ/�\0��\��)���\�\�*�+\�\�B�Z���\�5%\�\�\�	|�n g��8\�H`p\�\�\�	\�re��7�\�j\nD�xn8�ʪ	.\�p\�0�b�A7|�\�1�_�->F/\"\�䊅�f��zQ�\�YͶ`\�\�Y�j?\0|\�\�>w�}`\n9@t\�J��;��r`��\�6��9|�jG�e\�V	��S\'K\�f�\�_r�	\���\�;b��;3�\�\�Ta��D�u{��\�\'�\�5\�Ta \����&�L��\�ϐ��^\�\���HƐ�t�\��	-\��\�(��V/0I��\�ѝ��Ɨg�X\�f\n�@9`�۶M)\�\�87H\�b<��\n�u\��#����mGnbh�3s[��r�1a�r�Gjkߨ\�I�R�\��\�4,�4�o_\�:s�]h���O	\�\�b68\0\�Â���y��#t\0NSP�\��\�\�\�uv \�\n����!䐉/�w����eTwj-\�N\�d�R\�ᕲ,�|\�`\�6K1�`[Un\�)v�i\reT���\�N�0!\�\�N\�f�<�L�\�dYj�����\r�i\�\'�N�\0}9\�^B\�]�S\�0A;��\�C+(o\�<W8ZC�>s��m\��^A�G\�H�@�k�+\��^7Vn�\�H%��\�<AɁdY�Z�I�u�ۢ���dv�I R\�\�\rT�oYH�\�K\����L\����k�0cQ(��\�;�\�q�@kU\�Gk�G�C|\�ք}��Ĕ\�_�c\ZN�P�\�\�\�`� }�x\�VQ�\�A@\�\�ᡱGd�\�@�\�u�X�����\��Z\�\�)�\�^DU\�U\�(�s\�\�&�J��N���vA�Bv�Q}\�\�`ޢ���o\nh\�\�3�\��L��]�C���C\��S~Z�MQ��sU��@k�cE�xRB\�\0����oh�U\�#:$x�\�<��n�UTwv[�3Z�ㆫ�\�\�G�\\A5y�,��ڡ\�f`У�J�mA�!k\'�-��PrX	\�戎A�򴩠L�\�_�\������M��}\\\�/�\�\�㲑U\�j�*\�0��FOM~\�	ڍ\�#�N\�\�mAٰ���\�I��l\�\�\�Ģg\�T\���ŧ\����B�uL ��-�	��F�{a�>&\� \���,�\�\�M5t��5\�즺3�[��l��m�6\�v\�!��\��Qj\�A\���\�\�Q\�>y�\�\��\�xy�No\�\�vYDjE]P\'\�-�z�\�9\�\Z\�r\�8�A��\�3\�ݮ�D�yE\�e+\�ޓ�:���3\�.a\�?d`\�a�����\n]	ݩ̦Fo&\�_�*�t\'Du\Z@hm���?W!�S�l���\��ڸOd��ꠋkKh��J\���3\�4�V�~Q�:\�FR\�}�D\�L\Z��E	K��7\'>w\�)@�\�/�ez\�\0K�\Z<)�__w��x2Za	1���7}13��\r\�l3TXB\�<�X:���\�\�\�\��5�ѐA1�<^$XB\\�(�/�|�13�^\'�S)�\�\�L�N�\�w\�\��\�N\�\�\nꈺ�~0\�x���N���\�C�����N�\�yE�:�\�6�!\�\�M\�\nK�\�\�L{\�\r*ؽ�U����|��_RBB\�\�>;`	��\�n�gP\��dV��lGll�ۃ%\�N/�\Z7\�\�~\�t˵m�R}t����j�4���ֱ\�F\�̠�\�6 .���=�\�[��[�#\Z���\�йs\�Z\����\r\�ϕ6\r\��cf�AX�Q~���\�FѢ�N�\�[gIaj��0�\�e��S[fɲa\�C\�\�\�\�s\0�\�?L\�C\�L3\��5m�;���[(�\�\�lp�@^\�y��\�/1�s\�-�W�г\'%%>LOHoN|�f�=��\�]JK��\�>��K�\��*&<\�������\"\�<��Z�[�?�S����7�\0\0\0\0IEND�B`�');
/*!40000 ALTER TABLE `cards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students`
--

DROP TABLE IF EXISTS `students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `gradelist` varchar(1000) NOT NULL,
  `cardid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cardid_UNIQUE` (`cardid`),
  CONSTRAINT `cardid` FOREIGN KEY (`cardid`) REFERENCES `cards` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students`
--

LOCK TABLES `students` WRITE;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
INSERT INTO `students` VALUES (2,'Jerry','A+,B+,c+',1);
/*!40000 ALTER TABLE `students` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-02-25 13:32:43
