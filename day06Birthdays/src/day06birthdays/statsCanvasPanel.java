
package day06birthdays;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 *
 * @author 6155202
 */
public class statsCanvasPanel extends javax.swing.JPanel {

    /**
     * Creates new form statsCanvasPanel
     */
    //public int []  monthlyStats=new int[12];
    public int []  monthlyStats={7,8,0,4,7,2,6,4,3,8,0,1};
    public statsCanvasPanel() {
        //initComponents();
    }
    @Override
    public Dimension getPreferredSize() {
        return new Dimension(300, 100);
    }

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);//Has To Stay..Never Remove It
        Graphics2D g2d = (Graphics2D) g.create();
        int point=0;
        for(int monthNum=0;monthNum<12;monthNum++) {
            
            g2d.drawRect(point, 100-monthlyStats[monthNum]*10,25,100);
            point+=25;
        }
        g2d.drawLine(0, getHeight() , getWidth(), getHeight() );
        g2d.dispose();
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
