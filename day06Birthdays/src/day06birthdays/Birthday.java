/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day06birthdays;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import static java.time.temporal.TemporalQueries.localDate;

import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author 6155202
 */
public class Birthday {
    private String name;
    private Date bd;
     final static SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd");
    //from line constructor
    //setters and getters
    //overridden toString
    //toDataString()
    //Constructor
    public Birthday(String name, Date bd) {
        setName(name);
        setBd(bd);
        
    }
    //from line constructor
     public Birthday(String dataLine) {
        try {
            String[] data = dataLine.split(";");
            if (data.length != 3) {
                throw new IllegalArgumentException("Invalid number of items in data line");
            }
            setName(data[0]); // ex
            setBd(dateFormat.parse(data[1])); // ex NumberFormatException
        } catch (NumberFormatException |ParseException ex) {
            throw new IllegalArgumentException("Parsing error");
        }
    }
     //setters and getters

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBd() {
        return bd;
    }

    public void setBd(Date bd) {
        this.bd = bd;
    }
      //overridden toString

    @Override
    public String toString() {
        return String.format("%s born in %s", name,bd);
    }
    //toDataString()
    String toDataString() {
        return String.format("%s;%d", name,  dateFormat.format(bd));
    }
   /*public int tillBD(){
Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
Date date = Date.from(instant);
  
   
   }*/ 
}
