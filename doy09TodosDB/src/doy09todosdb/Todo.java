/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doy09todosdb;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
enum Status {
      Pending,Done,Delegated
    }

/**
 *
 * @author 6155202
 */
class InvalidDataException extends Exception {

    InvalidDataException(String msg) {
        super(msg);
    }
}

public class Todo {

    private int id;
    private String task;// 1-100 characters, any characters
    private int difficulty;// 1-5
    private Date dateDue;// any valid date where year is 1900 to 2100 both inclusive
    private Status status;
    SimpleDateFormat withDashFormat=new SimpleDateFormat("yyyy-MM-dd");
    enum Status {
         Pending, Done, Delegated
    }
    //constructor
    public Todo(int id, String task, int difficulty, Status status, Date dateDue) throws InvalidDataException {
        setId(id);
        setTask(task);
        setDifficulty(difficulty);
       
        setStatus(status);
         setDateDue(dateDue);
    }
//setters and getters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) throws InvalidDataException {
        if(task.length()<1||task.length()>100){
            throw new InvalidDataException("task showld be 1-100 characters");
        }
        this.task = task;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public Date getDateDue() {
        return dateDue;
    }

    public void setDateDue(Date dateDue) {//1900 to 2100
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateDue);
        int year = cal.get(Calendar.YEAR);
        if (year < 1900 | year > 2100) {
            throw new IllegalArgumentException("Year must be in 1900-2100 range");
        }
        this.dateDue = dateDue;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    
//ToString method

    @Override
    public String toString() {
        String d=withDashFormat.format(dateDue);
        return String.format("%s by %s/diff.%s ,%s",task,d,difficulty+"",status+"");
    }
    
    

}
