package doy09todosdb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author 6155202
 */
public class Database {

    private Connection dbConn;

    Database() throws SQLException {
        try {// Setup the connection with the DB
            // Class.forName("com.mysql.jdbc.Driver");
            dbConn = DriverManager.getConnection("jdbc:mysql://localhost/ipd20todos?"
                    + "user=root&password=opel2003");
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println("database error");
            System.exit(1); // FATAL ERROR, EXIT PROGRAM
        } 
    }

    ArrayList<Todo> getAllTodos() throws SQLException, InvalidDataException {
        ArrayList<Todo> list = new ArrayList<Todo>();

        Statement stmtSelect = dbConn.createStatement();
        // Result set get the result of the SQL query
        ResultSet resultSet = stmtSelect.executeQuery("SELECT * FROM todos");
        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String task = resultSet.getString("task");
            int difficulty = resultSet.getInt("difficulty");

            String stat = resultSet.getString("status");
            Date due = resultSet.getDate("dueDate");
            Todo todo = new Todo(id, task, difficulty, Todo.Status.valueOf(stat), due);
            list.add(todo);

        }
        return list;
    }

    void addTodo(Todo todo) throws SQLException {

        PreparedStatement stmtInsert = dbConn
                .prepareStatement("INSERT INTO todos VALUES (NULL, ?, ?,?,?)");
        // Parameters start with 1
        stmtInsert.setString(1, todo.getTask());
        stmtInsert.setInt(2, todo.getDifficulty());
        java.sql.Date dueDate = new java.sql.Date(todo.getDateDue().getTime());
        stmtInsert.setDate(4, dueDate);
        stmtInsert.setString(3, todo.getStatus() + "");
        stmtInsert.executeUpdate();
    }

    void updateTodo(Todo todo) throws SQLException {
        PreparedStatement stmtUpdate = dbConn
                .prepareStatement("UPDATE todos SET task=?, difficulty=?,dueDate=?,status=? WHERE id=?");
        // Parameters start with 1
        stmtUpdate.setString(1, todo.getTask());
        stmtUpdate.setInt(2, todo.getDifficulty());
        java.sql.Date dueDate = new java.sql.Date(todo.getDateDue().getTime());
        stmtUpdate.setDate(3, dueDate);
        stmtUpdate.setString(4, todo.getStatus() + "");
        stmtUpdate.setInt(5, todo.getId());
        stmtUpdate.executeUpdate();
    }

    void deleteTodo(int id) throws SQLException {
        PreparedStatement stmtDelete = dbConn.prepareStatement("DELETE FROM todos WHERE id=?");
        stmtDelete.setInt(1, id);
        stmtDelete.executeUpdate();
        //System.out.println(id);
    }

    Todo selectTOdoWhere(int id) throws SQLException, InvalidDataException {
        // Result set get the result of the SQL query
        String task = null;
        int difficulty = 0;
        Date date = null;
        Todo.Status status = null;
        Todo t = null;
        PreparedStatement stmtSelect = dbConn.prepareStatement("SELECT * FROM todos WHERE id=?");
        stmtSelect.setInt(1, id);
        ResultSet resultSet = stmtSelect.executeQuery();
        while (resultSet.next()) {
            int idenetity = resultSet.getInt("id");
            task = resultSet.getString("task");
            difficulty = resultSet.getInt("difficulty");
            date = resultSet.getDate("dueDate");
            status = Todo.Status.valueOf(resultSet.getString("status"));
            t = new Todo(id, task, difficulty, status, date);
        }
        return t; 
    }
}
