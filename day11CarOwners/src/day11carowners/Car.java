/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day11carowners;

/**
 *
 * @author AQ-37
 */
public class Car {

    private int id;
    private int ownerId;
    private String makeModel;
    private int prodYear;
    private String plates;
    private String ownerName; // computed, not stored in Car table
//CONSTRUCTOR

    public Car(int id, int ownerId, String makeModel, int prodYear, String plates, String ownerName) {
        setId(id);
        setOwnerId(ownerId);
        setMakeModel(makeModel);
        setProdYear(prodYear);
        setPlates(plates);
        setOwnerName(ownerName);
    }
    //SEETERS AND GETTERS

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    public String getMakeModel() {
        return makeModel;
    }

    public void setMakeModel(String makeModel) {
        this.makeModel = makeModel;
    }

    public int getProdYear() {
        return prodYear;
    }

    public void setProdYear(int prodYear) {
        this.prodYear = prodYear;
    }

    public String getPlates() {
        return plates;
    }

    public void setPlates(String plates) {
        this.plates = plates;
    }
//FETCH FROM THE OTHER TABLE
    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }
    //OVELOADDED TOSTRING

    @Override
    public String toString() {
        return String.format("%d:%s,%d,%s,%s",id,makeModel,prodYear,(ownerName==null)? "Has no owner":ownerName,(plates==null)?"No plates":plates);
    }
    
}
