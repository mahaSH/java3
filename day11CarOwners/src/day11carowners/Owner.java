/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day11carowners;

/**
 *
 * @author AQ-37
 */
public class Owner {
    private int id;
    private String name;
    private byte[] photo;
    private int carsOwned; // computed, not stored
    
    //CONSTRUCTROR

    public Owner(int id, String name, byte[] photo, int carsOwned) {
        setId(id);
        setName(name);
        setPhoto(photo);
        setCarsOwned(carsOwned);
    }
    //SETTERS AND GETTERS

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public int getCarsOwned() {
        return carsOwned;
    }

    public void setCarsOwned(int carsOwned) {//COMPUTE CARSOWNED
        this.carsOwned = carsOwned;
    }
    //OVERLOADDED TOsTRING

    @Override
    public String toString() {
        return String.format("%d:%s owns %d cars.",id,name,carsOwned);
    }
    
}
