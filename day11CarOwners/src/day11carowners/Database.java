/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day11carowners;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.DefaultListModel;
import java.util.ArrayList;
import javax.swing.DefaultListModel;

class UniqeValueRequiredException extends Exception {
    public UniqeValueRequiredException(String msg) {
        super(msg);
    }
}

public class Database {
    private Connection dbConn;

    Database() throws SQLException {
     try {// Setup the connection with the DB
           dbConn = DriverManager.getConnection("jdbc:mysql://localhost/day10carowners?"
                    + "user=root&password=root");
            /* dbConn = DriverManager.getConnection("jdbc:mysql://localhost/day10carowners?"
                    + "user=root&password=root");*/
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println("database error");
            System.exit(1);
        }
    }
    //CARS DATABASE FUNCTIONS
    

    ArrayList<Car> getCars() throws SQLException {
        ArrayList<Car> list = new ArrayList<Car>();

        Statement stmtSelect = dbConn.createStatement();
        // Result set get the result of the SQL query
        ResultSet resultSet = stmtSelect.executeQuery("SELECT c.id,c.ownerid,c.makemodel,c.prodyear,c.plates,o.name FROM cars as c inner join owners as o on c.ownerid=o.id ");
        while (resultSet.next()) {
            int id = resultSet.getInt("c.id");
            int ownerId = resultSet.getInt("c.ownerid");
            String makeModel = resultSet.getString("c.makemodel");
            int prodyear = resultSet.getInt("c.prodyear");
            String plates = resultSet.getString("c.plates");
            String name=resultSet.getString("o.name");
            Car car=new Car(id,ownerId,makeModel,prodyear,plates,name);
            list.add(car);

        }
        return list;
    }

    void addCar(Car car) throws SQLException, UniqeValueRequiredException {
        // select first to check that plates are not used yet
       
        int id= 0;
         String foundPlates=null;
         boolean plateDuplicate=false; 
        PreparedStatement stmtCheck = dbConn
                .prepareStatement("SELECT id FROM cars WHERE palte=?");
        stmtCheck.setString(1, car.getPlates());
        ResultSet resultSet = stmtCheck.executeQuery();
         while (resultSet.next()) {
              // or that the only one with those plates is car.id
         id= resultSet.getInt("c.id");
          foundPlates= resultSet.getString("c.plates");
          if(foundPlates!=null&&id!=car.getId()){
             throw new UniqeValueRequiredException("plate is already taken,no duplication allowded") ;
          }
        }
        //IF NO DUPLICATION,ADD THE CAR
        PreparedStatement stmtInsert = dbConn
                .prepareStatement("INSERT INTO cars VALUES (NULL, ?, ?,?,?)");
        // Parameters start with 1
        stmtInsert.setInt(1, car.getOwnerId());
        stmtInsert.setString(2, car.getMakeModel());
        stmtInsert.setInt(3, car.getProdYear());
        stmtInsert.setString(4, car.getPlates());
        stmtInsert.executeUpdate();
    }

    void updateCar(Car car) throws SQLException, UniqeValueRequiredException {
        // select first to check that plates are not used yet
        // throw UniqeValueRequiredException if check fails
        // select first to check that plates are not used yet
       
        int id= 0;
         String foundPlates=null;
         boolean plateDuplicate=false; 
        PreparedStatement stmtCheck = dbConn
                .prepareStatement("SELECT id FROM cars WHERE palte=?");
        stmtCheck.setString(1, car.getPlates());
        ResultSet resultSet = stmtCheck.executeQuery();
         while (resultSet.next()) {
              // or that the only one with those plates is car.id
         id= resultSet.getInt("c.id");
          foundPlates= resultSet.getString("c.plates");
          if(foundPlates!=null&&id!=car.getId()){
             throw new UniqeValueRequiredException("plate is already taken,no duplication allowded") ;
          }
        }
        //IF NO DUPLICATION,UPDATE THE CAR
        PreparedStatement stmtUpdateCar = dbConn
                .prepareStatement("UPDATE cars  SET ownerid=?,makemodel=?,prodyear=?,plates=? WHERE id=?");
        // Parameters start with 1
        stmtUpdateCar.setInt(1, car.getOwnerId());
        stmtUpdateCar.setString(2, car.getMakeModel());
        stmtUpdateCar.setInt(3, car.getProdYear());
        stmtUpdateCar.setString(4, car.getPlates());
        stmtUpdateCar.setInt(5, car.getId());
        stmtUpdateCar.executeUpdate();
        PreparedStatement stmtUpdateOwner = dbConn
                .prepareStatement("UPDATE owners  SET name=? WHERE id=?");
         stmtUpdateOwner.setString(1, car.getOwnerName());
        stmtUpdateOwner.setInt(5, car.getOwnerId());
        stmtUpdateOwner.executeUpdate();
    }

    void deletecAR(int id) throws SQLException {
        PreparedStatement stmtDelete = dbConn.prepareStatement("DELETE FROM cars WHERE id=?");
        stmtDelete.setInt(1, id);
        stmtDelete.executeUpdate();
        //System.out.println(id);
    }

    Car selectCarWhereId(int id) throws SQLException {
        // Result set get the result of the SQL query
        int ownerId= 0;
        String makeModel = null;
        int prodYear = 0;
        String plates = null;
        String ownerName=null;
        Car car=null;
        PreparedStatement stmtSelect = dbConn.prepareStatement("SELECT c.id,c.ownerid,c.makemodel,c.plates,c.prodyear,o.name FROM cars as c inner join owners as o on c.ownerid=o.id WHERE id=?");
        stmtSelect.setInt(1, id);
        ResultSet resultSet = stmtSelect.executeQuery();
        while (resultSet.next()) {
          makeModel= resultSet.getString("c.makemodel");
          plates= resultSet.getString("c.plates");
          ownerId= resultSet.getInt("c.ownerid");
          prodYear= resultSet.getInt("c.prodyear");
          ownerName=resultSet.getString("o.name");
         car = new Car(id, ownerId,makeModel,prodYear,plates,ownerName);
        }
        return car;
    }
     
//OWNERS DATABASE FUNCTIONS
    int countNumberOfCars(int i) throws SQLException{ 
        int numberOfCars=0;
        Statement stmtSelect = dbConn.createStatement();
           PreparedStatement stmtSelectCount = dbConn.prepareStatement("select c.ownerId ,count(c.id) \n" +
"FROM cars as c \n" +
"inner join owners as o \n" +
"on o.id=c.ownerID \n" +
"where o.id=? \n" +
"group by c.ownerID" );
        stmtSelectCount.setInt(1, i);
            ResultSet resultSetCount = stmtSelectCount.executeQuery();
            while (resultSetCount.next()){
              numberOfCars=resultSetCount.getInt("count(c.id)");  
            }
        return( numberOfCars); 
    }

    ArrayList<Owner> getOwners() throws SQLException {
        ArrayList<Owner> list = new ArrayList<Owner>();
        Statement stmtSelect = dbConn.createStatement();
        // Result set get the result of the SQL query
       ResultSet resultSet = stmtSelect.executeQuery("SELECT * FROM owners");
          
        while (resultSet.next()) { 
           int id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            byte[] photo=resultSet.getBytes("photo");
            int numberOfCars=countNumberOfCars(id);
            Owner owner=new Owner(id,name,photo,numberOfCars);
            list.add(owner);
        }
        return list;
    }

    void addOwner(Owner owner) throws SQLException {
        PreparedStatement stmtInsert = dbConn
                .prepareStatement("INSERT INTO owners VALUES (NULL, ?, ?)");
        // Parameters start with 1
        stmtInsert.setString(1, owner.getName());
        stmtInsert.setBytes(2, owner.getPhoto());
        stmtInsert.executeUpdate();
    }

   /* void updateBook(Car car) throws SQLException {
        PreparedStatement stmtUpdate = dbConn
                .prepareStatement("UPDATE cars  SET ownerid=?,makemodel=?,prodyear=?,plates=? WHERE id=?");
        // Parameters start with 1
        stmtUpdate.setInt(1, car.getOwnerId());
        stmtUpdate.setString(2, car.getMakeModel());
        stmtUpdate.setInt(3, car.getProdYear());
        stmtUpdate.setString(4, car.getPlates());
        stmtUpdate.executeUpdate();
    }

    void deleteBook(int id) throws SQLException {
        PreparedStatement stmtDelete = dbConn.prepareStatement("DELETE FROM cars WHERE id=?");
        stmtDelete.setInt(1, id);
        stmtDelete.executeUpdate();
        //System.out.println(id);
    }

    Car selectCarWhereId(int id) throws SQLException {
        // Result set get the result of the SQL query
        int ownerId= 0;
        String makeModel = null;
        int prodYear = 0;
        String plates = null;
        String ownerName=null;
        Car car=null;
        PreparedStatement stmtSelect = dbConn.prepareStatement("SELECT c.id,c.ownerid,c.makemodel,c.prodyear,c.prodyear,o.name FROM cars as c inner join owners as o on c.ownerid=o.id WHERE id=?");
        stmtSelect.setInt(1, id);
        ResultSet resultSet = stmtSelect.executeQuery();
        while (resultSet.next()) {
          makeModel= resultSet.getString("c.id");
          plates= resultSet.getString("c.ownerid");
          ownerId= resultSet.getInt("c.makemodel");
          prodYear= resultSet.getInt("c.prodyear");
          ownerName=resultSet.getString("o.name");
         car = new Car(id, ownerId,makeModel,prodYear,plates,ownerName);
        }
        return car;
    }
    /*ArrayList<Owner> getAllOwners() {
        // return owners and number of cars they own
        // SELECT *,COUNT(*) as carsOwned JOIN Owners onto Cars, GROUP BY ownerId
        
    }
    
    ArrayList<Car> getAllCars() {
        // return cars and possibly their owner names
        // SELECT * LEFT OUTER JOIN Cars onto Owner
    }

    ArrayList<Car> getAllCarsOwnedBy(int ownerId) {
        // return cars owned by a specific owner
        // SELECT * JOIN Cars onto Owner
    }

    
    void updateOwner(Owner owner) {
        // select first to check that owner name is not used yet
        // throw UniqeValueRequiredException if check fails
    }
    
    void addOwner(Owner owner) {
        // select first to check that owner name is not used yet
        // or that the only one with that name is owner.id
        // throw UniqeValueRequiredException if check fails
    }

    void updateCar(Car car) {
        
    }
    
    void addCar(Car car) {
        // select first to check that plates are not used yet
        // or that the only one with those plates is car.id
        // throw UniqeValueRequiredException if check fails
    }*/
    
    void deleteCar(int id) {
        
    }

    void deleteOwner(int id) {
        // consider database settings, one of:
        // RESTRICT | CASCADE | SET NULL (preferred in this case) | NO ACTION | SET DEFAULT
        
        
    }
    
    // generic method
    static <T> void copyToModel(ArrayList<T> list, DefaultListModel<T> model) {
        model.clear();
        for (T item : list) {
            model.addElement(item);
        }
    }
    
}

