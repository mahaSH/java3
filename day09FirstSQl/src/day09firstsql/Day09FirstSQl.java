/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day09firstsql;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

/**
 *
 * @author ipd
 */
public class Day09FirstSQl {

    static Scanner input = new Scanner(System.in);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            // Class.forName("com.mysql.jdbc.Driver");
            // Setup the connection with the DB
            Connection connection = DriverManager
                    .getConnection("jdbc:mysql://localhost/ipd20people?"
                            + "user=root&password=root");
            {
                // insert data user provided
                System.out.print("Enter name: ");
                String name = input.nextLine();
                System.out.print("Enter age: ");
                int age = input.nextInt();
                input.nextLine();

                //
                PreparedStatement stmtInsert = connection
                        .prepareStatement("INSERT INTO people VALUES (NULL, ?, ?)");
                // Parameters start with 1
                stmtInsert.setString(1, name);
                stmtInsert.setInt(2, age);
                stmtInsert.executeUpdate();
                System.out.println("Record inserted");
            }
            // select
            {
                Statement stmtSelect = connection.createStatement();
                // Result set get the result of the SQL query
                ResultSet resultSet = stmtSelect.executeQuery("SELECT * FROM people");
                while (resultSet.next()) {
                    int id = resultSet.getInt("id");
                    String name = resultSet.getString("name");
                    int age = resultSet.getInt("age");
                    System.out.printf("%d: %s is %d y/o\n", id, name, age);
                }
            }
            { // update
                System.out.print("Enter id of the record you wish to update: ");
                int id = input.nextInt();
                input.nextLine();
                System.out.print("Enter new name: ");
                String name = input.nextLine();
                System.out.print("Enter new age: ");
                int age = input.nextInt();
                input.nextLine();
                //
                PreparedStatement stmtUpdate = connection
                        .prepareStatement("UPDATE people SET name=?, age=? WHERE id=?");
                // Parameters start with 1
                stmtUpdate.setString(1, name);
                stmtUpdate.setInt(2, age);
                stmtUpdate.setInt(3, id);
                stmtUpdate.executeUpdate();
                // FIXME: if the record did not exist it will not be considered an error by SQL
                System.out.println("Record updated");
                // 
            }
            {
                System.out.print("Enter id of the record you wish to delete: ");
                int id = input.nextInt();
                input.nextLine();
                PreparedStatement stmtDelete = connection.prepareStatement("DELETE FROM people WHERE id=?");
                stmtDelete.setInt(1, id);
                stmtDelete.executeUpdate();
                // FIXME: if the record did not exist it will not be considered an error by SQL
            }
        //} catch (ClassNotFoundException ex) {
         //   Logger.getLogger(Day09FirstSQL.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            //Logger.getLogger(Day09FirstSQL.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
