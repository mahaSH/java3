/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz03final;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author 6155202
 */
public class Database {
     private Connection dbConn;

    Database() throws SQLException {
        try {// Setup the connection with the DB
           dbConn = DriverManager.getConnection("jdbc:mysql://localhost/final?"
                    + "user=root&password=opel2003");
            /* dbConn = DriverManager.getConnection("jdbc:mysql://localhost/day10library?"
                    + "user=root&password=root");*/
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.out.println("database error");
            System.exit(1);
        }
    }
    /**************************************************************************************/
    void addStudent(Student s) throws SQLException {

        PreparedStatement stmtInsert = dbConn
                .prepareStatement("INSERT INTO students VALUES (NULL, ?, ?,?)");
        // Parameters start with 1
        stmtInsert.setString(1, s.getName());
        stmtInsert.setString(2, s.getGradeList());
        stmtInsert.setInt(3, s.getCardId());
        stmtInsert.executeUpdate();
    }
    /**************************************************************************************/
    ArrayList<Student> getAllStudents() throws SQLException {
        ArrayList<Student> Studentlist = new ArrayList<Student>();

        Statement stmtSelect = dbConn.createStatement();
        // Result set get the result of the SQL query
        ResultSet resultSet = stmtSelect.executeQuery("SELECT s.* , c.permcode FROM students AS s LEFT JOIN cards AS c on s.cardId=c.id  ");
        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            String gradeList = resultSet.getString("gradelist");
            int cardId = Integer.parseInt(resultSet.getString("id"));
            Student student=new Student(id,name,gradeList,cardId);
            Studentlist.add(student);

        }
        return Studentlist;
    }
    /**************************************************************************************/
    int addCard(Card card) throws SQLException {

        PreparedStatement stmtInsert = dbConn
                .prepareStatement("INSERT INTO cards VALUES (NULL, ?, ?)",
                        Statement.RETURN_GENERATED_KEYS);
        // Parameters start with 1
        stmtInsert.setString(1, card.getPermCode());
        stmtInsert.setBytes(2, card.getPhoto());
        stmtInsert.executeUpdate();
        ResultSet rs = stmtInsert.getGeneratedKeys();
        if (rs.next()) {
            int lastInsertedId = rs.getInt(1);
            return lastInsertedId;
        } else {
            throw new SQLException("Error getting last insert id");
        }
    }
}
