/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz03final;

/**
 *
 * @author 6155202
 */
public class Student {

    private int id;
    private String name;//not null,max 100char
    private String gradeList;//not null max1000char
    private Integer cardId;//Null,unique
    //CONSTRUCTOR

    public Student(int id, String name, String gradeList, int cardId) {
        setId(0);
        setName(name);
        setGradeList(gradeList);
        setCardId(cardId);
    }
    //SETTERS AND GETTERS

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws IllegalArgumentException {
        if (name.length() > 100) {
            throw new IllegalArgumentException("name should not excexeds 100 char length");
        }
        this.name = name;
    }

    public String getGradeList() {
        return gradeList;
    }

    public void setGradeList(String gradeList) throws IllegalArgumentException {

        if (gradeList.length() > 1000) {
            throw new IllegalArgumentException("Grade list should not excexeds 1000 char length");
        }
        this.gradeList = gradeList;
    }

    public Integer getCardId() {
        return cardId;
    }

    public void setCardId(Integer cardId) {
        this.cardId = cardId;
    }
    //TOSTRING METHOD

    @Override
    public String toString() {
        return String.format("%d:%s (%s) GPA=%.2f",id,name,(cardId == null)?"No Record":cardId,getGPA());
    }
    double getGPA() {
        String[] data = gradeList.split("[,|\\t|\\n|-]");//[,\\t\\n-]
        double sum = 0;
        for (int i = 0; i < data.length; i++) {
          System.out.printf(data[i]);
            switch (data[i]) {
                case "A+":
                    sum += 4.00;
                    break;
                case "A-":
                    sum += 3.70;
                     break;
                case "B+":
                    sum += 3.33;
                     break;
                case "B":
                    sum += 3.00;
                     break;
                case "B-":
                    sum += 2.70;
                     break;
                case "c+":
                    sum += 2.30;
                     break;
                case "C":
                    sum += 2.00;
                     break;
                case "C-":
                    sum += 1.70;
                     break;
                case "D+":
                    sum += 1.30;
                     break;
                case "D":
                    sum += 1.00;
                     break;
                case "D=":
                    sum += 0.70;  
                     break;
                default:
                 throw new IllegalArgumentException("not valid degree list");
            }
        }
        return(sum/data.length);
    }
}
