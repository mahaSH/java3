/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz03final;

/**
 *
 * @author 6155202
 */
public class Card {

    private int id;
    private String permCode;//4 digits follwed by 10 numbers
    private byte[] photo;
    //CONSTRUCTOR

    public Card(int id, String permCode, byte[] photo) {
        setId(id);
        setPermCode(permCode);
        setPhoto(photo);
    }
    //SETTERS AND GETTERS

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPermCode() {

        return permCode;
    }

    public void setPermCode(String permCode) {
        /*if(!permCode.matches("[A-Z]{4}\d{10}")){
              throw new IllegalArgumentException("permcode should be four capital letters followed by 10 digits");
         }*/
        this.permCode = permCode;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

}
