package quiz2employees;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

/**
 *
 * @author 6155202
 */
public class dlgStats_PanelCanvas extends javax.swing.JPanel {

    public static String[] daysArray = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
    };

    /**
     * Creates new form dlgStats_PanelCanvas
     */
    public int[] DailylyStats;

    public dlgStats_PanelCanvas() {
        initComponents();
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(200, 200);
    }

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g.create();
        for (int DayNum = 0; DayNum < 7; DayNum++) {
            //int friendsNum = monthlyStats[monthNum];
            g2d.setColor(Color.YELLOW);
            // g2d.fillRect(DayNum * (300 / 12), 100 - friendsNum * (100 / 10), 22, friendsNum * (100 / 10) - 1);
            // draw month
            g2d.setColor(Color.BLACK);
            AffineTransform affineTransform = new AffineTransform();
            affineTransform.rotate(Math.toRadians(90), 0, 0);
            Font origFont = new Font("TimesRoman", Font.PLAIN, 10);
            Font rotatedFont = origFont.deriveFont(affineTransform);
            g2d.setFont(rotatedFont);
            g2d.drawString(daysArray[DayNum], DayNum * (300 / 12) + 4, 100 - 22);
            //
            g2d.setFont(new Font("TimesRoman", Font.BOLD, 16));
            //g2d.drawString(friendsNum + "", monthNum * (300 / 12) + 4, 100 - 26);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 250, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 103, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
