/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz2employees;

import java.awt.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

/**
 *
 * @author Maha M.Shawkat
 */
class InvalidValueException extends Exception {

    InvalidValueException(String msg) {
        super(msg);
    }
}

enum Weekday {
    Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday
}

public class EmployeeSchedule {

    private String name; // 2-50 characters, not permitted are: ;^?@!~*
    private boolean isManager;
    private String department; // 2-50 characters, not permitted are: ;^?@!~*
    private Date dateHired; // year between 1900 and 2100
    private ArrayList<Weekday> workdaysList; // no duplicates allowed
    final static SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd,yyyy");
    //Constructor

    public EmployeeSchedule(String name, boolean isManager, String department, Date dateHired, ArrayList<Weekday> workdaysList) throws InvalidValueException {
        setName(name);
        setIsManager(isManager);
        setDepartment(department);
        setDateHired(dateHired);

    }

    //from line constructor
    public EmployeeSchedule(String dataLine) throws InvalidValueException {
        try {
            String[] data = dataLine.split(";");
            if (data.length != 3) {
                throw new InvalidValueException("Invalid number of items in data line");
            }
            setName(data[0]); // ex
            if (data[0].substring(data[0].length() - 1) == "*") {
                setIsManager(true);
            } else {
                setIsManager(false);
            }

            setDepartment(data[2]);

            setDateHired(dateFormat.parse(data[1]));
            /*List<String> items = Arrays.asList(data[3].split(","));*/
        } catch (NumberFormatException | ParseException ex) {
            throw new InvalidValueException("Parsing error");

        }
    }
    //setters and getters

    public String getName() {
        return name;
    }

    public void setName(String name) throws InvalidValueException {// 2-50 characters, not permitted are: ;^?@!~*
        if (name.length() < 2 || name.length() > 50 || name.contains(";") || name.contains("^") || name.contains("?") || name.contains("@") || name.contains("!") || name.contains("~") || name.contains("*")) {
            throw new InvalidValueException("Name should be 2-50 characters, not permitted are: ;^?@!~*");
        }
        this.name = name;
    }

    public boolean isIsManager() {
        return isManager;
    }

    public void setIsManager(boolean isManager) {
        this.isManager = isManager;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) throws InvalidValueException {//2-50 characters, not permitted are: ;^?@!~*
        if (department.length() < 2 || department.length() > 50 || department.contains(";") || department.contains("^") || department.contains("?") || department.contains("@") || department.contains("!") || department.contains("~") || department.contains("*")) {
            throw new InvalidValueException("daprtment Name should be 2-50 characters, not permitted are: ;^?@!~*");
        }
        this.department = department;
    }

    public Date getDateHired() {

        return dateHired;
    }

    public void setDateHired(Date dateHired) throws InvalidValueException {//year between 1900 and 2100
        if (dateHired.getYear() > 2100 || dateHired.getYear() < 1900) {
            throw new InvalidValueException("hired year should be between 1900 and 2100");
        }
        this.dateHired = dateHired;
    }

    public ArrayList<Weekday> getWorkdaysList() {
        return workdaysList;
    }

    public void setWorkdaysList(ArrayList<Weekday> workdaysList) {
        this.workdaysList = workdaysList;
    }

    public static SimpleDateFormat getDateFormat() {
        return dateFormat;
    }

    //from array list to array function
    Weekday[] getWorkdays() {
        Weekday days[] = new Weekday[workdaysList.size()];
        days = workdaysList.toArray(days);
        return days;
    }

    //boolean isWorkingOn(Weekday weekday) { ... }
    boolean isWorkingOn(Weekday weekday) {
        for (int i = 0; i < workdaysList.size(); ++i) {
            if (workdaysList.get(i).equals(weekday)) {
                return true;
            }
        }
        return (false);
    }
    //void addWorkday(Weekday workday) { ... } 

    void addWorkday(Weekday workday) {
        for (int i = 0; i < workdaysList.size(); ++i) {
            if (workdaysList.get(i).equals(workday + "")) {
                return;
            }
        }
        workdaysList.add(workday);
    }
    //overridden toString

    @Override
    public String toString() {
        Weekday days[] = new Weekday[workdaysList.size()];
        days = workdaysList.toArray(days);
        return String.format("%s %s of %s,hired in %s,work on %s", name, (isManager) ? "Maneger" : "employee", department, dateFormat.format(dateHired), days.toString());
    }

    String toDataString() {
        return String.format("%s;%s;%s", name.concat((isManager) ? "*" : ""), department, dateFormat.format(dateHired));
    }

}
