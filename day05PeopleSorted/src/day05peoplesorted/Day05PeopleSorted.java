/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day05peoplesorted;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JWindow;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;

/**
 *
 * @author ipd
 */
public class Day05PeopleSorted extends javax.swing.JFrame {

    DefaultListModel<Person> modelPersonList = new DefaultListModel<Person>();
    String sortNameType = "Asc";
    String sortAgeType = "Asc";
    String sortWeightType = "Asc";

    /**
     * Creates new form Day05PeopleSorted
     */
    public Day05PeopleSorted() {
        initComponents();
        lblErrorName.setVisible(false);
        lblErrorAge.setVisible(false);
        lblErrorWeight.setVisible(false);

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        popupMenue = new javax.swing.JPopupMenu();
        DeleteSelected = new javax.swing.JMenuItem();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstPerson = new javax.swing.JList<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        tfName = new javax.swing.JTextField();
        tfAge = new javax.swing.JTextField();
        tfWeight = new javax.swing.JTextField();
        btAdd = new javax.swing.JButton();
        btByName = new javax.swing.JButton();
        btByAge = new javax.swing.JButton();
        btByWeight = new javax.swing.JButton();
        lblErrorName = new javax.swing.JLabel();
        lblErrorAge = new javax.swing.JLabel();
        lblErrorWeight = new javax.swing.JLabel();
        btUpdate = new javax.swing.JButton();
        btDelete = new javax.swing.JButton();

        DeleteSelected.setText("Delete selected item");
        DeleteSelected.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                DeleteSelectedMouseReleased(evt);
            }
        });
        popupMenue.add(DeleteSelected);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jScrollPane1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jScrollPane1MouseReleased(evt);
            }
        });

        lstPerson.setModel(modelPersonList);
        lstPerson.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        lstPerson.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstPersonMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                lstPersonMouseReleased(evt);
            }
        });
        lstPerson.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                lstPersonValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(lstPerson);

        jLabel1.setText("Name:");

        jLabel2.setText("Age:");

        jLabel3.setText("Weight [kg]:");

        tfName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tfNameKeyReleased(evt);
            }
        });

        btAdd.setText("Add person");
        btAdd.setEnabled(false);
        btAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btAddActionPerformed(evt);
            }
        });

        btByName.setText("By name▲");
        btByName.setEnabled(false);
        btByName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btByNameActionPerformed(evt);
            }
        });

        btByAge.setText("By age▲");
        btByAge.setEnabled(false);
        btByAge.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btByAgeActionPerformed(evt);
            }
        });

        btByWeight.setText("By weight▲");
        btByWeight.setEnabled(false);
        btByWeight.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btByWeightActionPerformed(evt);
            }
        });

        lblErrorName.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblErrorName.setForeground(new java.awt.Color(255, 0, 51));
        lblErrorName.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblErrorName.setText("Name must be 1-50 charcters, only some allowed");

        lblErrorAge.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblErrorAge.setForeground(new java.awt.Color(255, 0, 51));
        lblErrorAge.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblErrorAge.setText("Age must be 1-150");

        lblErrorWeight.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        lblErrorWeight.setForeground(new java.awt.Color(255, 0, 51));
        lblErrorWeight.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblErrorWeight.setText("Weight must be 0-500kg");

        btUpdate.setText("Update Selected");
        btUpdate.setEnabled(false);
        btUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btUpdateActionPerformed(evt);
            }
        });

        btDelete.setText("Delete Selected");
        btDelete.setEnabled(false);
        btDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btDeleteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 287, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(tfName)
                                    .addComponent(tfAge)
                                    .addComponent(tfWeight, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(lblErrorName, javax.swing.GroupLayout.DEFAULT_SIZE, 253, Short.MAX_VALUE)
                                    .addComponent(lblErrorAge, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lblErrorWeight, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btAdd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btDelete)
                                .addGap(10, 10, 10)
                                .addComponent(btUpdate))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btByName)
                        .addGap(16, 16, 16)
                        .addComponent(btByAge)
                        .addGap(18, 18, 18)
                        .addComponent(btByWeight)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btByName)
                    .addComponent(btByAge)
                    .addComponent(btByWeight))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 283, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblErrorName)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(tfName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(26, 26, 26)
                        .addComponent(lblErrorAge)
                        .addGap(1, 1, 1)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(tfAge, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(34, 34, 34)
                        .addComponent(lblErrorWeight)
                        .addGap(1, 1, 1)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(tfWeight, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btAdd)
                            .addComponent(btUpdate)
                            .addComponent(btDelete))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btAddActionPerformed
        String name = tfName.getText();
        int age = Integer.parseInt(tfAge.getText());
        double weight = Double.parseDouble(tfWeight.getText());

        try {
            Person p = new Person(name, age, weight);
            //lstPerson.addElement(p.toString());
            modelPersonList.addElement(p);
        } catch (ValueInvalidException ex) {
            //
        }

        tfName.setText("");
        tfAge.setText("");
        tfWeight.setText("");
        btByAge.setEnabled(true);
        btByName.setEnabled(true);
        btByWeight.setEnabled(true);
    }//GEN-LAST:event_btAddActionPerformed

    private void tfNameKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfNameKeyReleased
        btAdd.setEnabled(true);
        String name = tfName.getText();
        lblErrorName.setVisible(!Person.isNameValid(name));
    }//GEN-LAST:event_tfNameKeyReleased

    private void btByNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btByNameActionPerformed

        if (sortNameType == "Asc") {
            ArrayList<Person> peopleList = new ArrayList<>();
            for (Object o : modelPersonList.toArray()) {
                peopleList.add((Person) o);
            }
            Collections.sort(peopleList, Person.comparatorByName);
            modelPersonList.clear();
            //modelPersonList.addAll(peopleList);
            for (int i = 0; i < modelPersonList.size(); i++) {
             modelPersonList.addElement(peopleList.get(i));   
            }
            btByName.setText("By name".concat("\u25BC"));
            sortNameType = "Desc";
        } else {
            ArrayList<Person> peopleList = new ArrayList<>();
            for (Object o : modelPersonList.toArray()) {
                peopleList.add((Person) o);
            }
           //  Collections.reverse(peopleList, Person.comparatorByName);
            Collections.sort(peopleList, Person.comparatorByNameDesc);
            modelPersonList.clear();
           // modelPersonList.addAll(peopleList);
           for (int i = 0; i < modelPersonList.size(); i++) {
             modelPersonList.addElement(peopleList.get(i));   
            }
            btByName.setText("By name".concat("\u25B2"));
            sortNameType = "Asc";
        }
    }//GEN-LAST:event_btByNameActionPerformed

    private void lstPersonValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_lstPersonValueChanged
        btByAge.setEnabled(true);
        btByName.setEnabled(true);
        btByWeight.setEnabled(true);


    }//GEN-LAST:event_lstPersonValueChanged

    private void btByAgeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btByAgeActionPerformed

        if (sortAgeType == "Asc") {
            ArrayList<Person> peopleList = new ArrayList<>();
            for (Object o : modelPersonList.toArray()) {
                peopleList.add((Person) o);
            }
            Collections.sort(peopleList, Person.comparatorByAge);
            modelPersonList.clear();
            //modelPersonList.addAll(peopleList);
            for (int i = 0; i < modelPersonList.size(); i++) {
             modelPersonList.addElement(peopleList.get(i));   
            }
            btByAge.setText("By age".concat("\u25BC"));
            sortAgeType = "Desc";
        } else {
            ArrayList<Person> peopleList = new ArrayList<>();
            for (Object o : modelPersonList.toArray()) {
                peopleList.add((Person) o);
            }
            Collections.sort(peopleList, Person.comparatorByAgeDesc);
            modelPersonList.clear();
            //modelPersonList.addAll(peopleList);
            for (int i = 0; i < modelPersonList.size(); i++) {
             modelPersonList.addElement(peopleList.get(i));   
            }
            btByAge.setText("By age".concat("\u25B2"));
            sortAgeType = "Asc";
        }
    }//GEN-LAST:event_btByAgeActionPerformed

    private void btByWeightActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btByWeightActionPerformed
        if (sortWeightType == "Asc") {
            ArrayList<Person> peopleList = new ArrayList<>();
            for (Object o : modelPersonList.toArray()) {
                peopleList.add((Person) o);
            }
            Collections.sort(peopleList, Person.comparatorByWeight);
            modelPersonList.clear();
            
            //modelPersonList.addAll(peopleList);
            for (int i = 0; i < modelPersonList.size(); i++) {
             modelPersonList.addElement(peopleList.get(i));   
            }
            btByWeight.setText("By weight".concat("\u25BC"));
            sortWeightType = "Desc";
        } else {
            ArrayList<Person> peopleList = new ArrayList<>();
            for (Object o : modelPersonList.toArray()) {
                peopleList.add((Person) o);
            }
            Collections.sort(peopleList, Person.comparatorByWeightDesc);
            modelPersonList.clear();
            //modelPersonList.addAll(peopleList);
            for (int i = 0; i < modelPersonList.size(); i++) {
             modelPersonList.addElement(peopleList.get(i));   
            }
            btByWeight.setText("By weight".concat("\u25B2"));
            sortWeightType = "Asc";
        }

    }//GEN-LAST:event_btByWeightActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        //Load from file 
        try ( Scanner fileInput = new Scanner(new File("peopleSorted.txt"))) {
            String errorListStr = "";
            while (fileInput.hasNextLine()) {
                String line = "";
                try {
                    line = fileInput.nextLine();
                    Person person = new Person(line);
                    modelPersonList.addElement(person);
                } catch (IllegalArgumentException | ValueInvalidException ex) {
                    errorListStr += String.format("Error: %s in line: %s\n", ex.getMessage(), line);

                }
                if (!errorListStr.isEmpty()) {
                    JOptionPane.showMessageDialog(this,
                            "There were errors paring data from file:\n" + errorListStr,
                            "Input error",
                            JOptionPane.ERROR_MESSAGE);
                }

            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Day05PeopleSorted.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_formWindowOpened

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        //Save before exiting
        try (PrintWriter fileOutput = new PrintWriter("peopleSorted.txt")) {
            for (Object o : modelPersonList.toArray()) {
                Person p=(Person)o;
                String line=p.toDataString();
                fileOutput.println(line);
            }
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this,
                "Error writing to file:\n" + ex.getMessage(),
                "File access error",
                JOptionPane.INFORMATION_MESSAGE);
        }
    
    }//GEN-LAST:event_formWindowClosing

    private void lstPersonMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstPersonMouseReleased
       btDelete.setEnabled(true);
       btUpdate.setEnabled(true);
    }//GEN-LAST:event_lstPersonMouseReleased

    private void btDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btDeleteActionPerformed
      int selected = lstPerson.getSelectedIndex();
       if (selected == -1) {
            return; // should never happen
        }
        modelPersonList.remove(selected);
    }//GEN-LAST:event_btDeleteActionPerformed

    private void btUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btUpdateActionPerformed
       Person person = lstPerson.getSelectedValue();
        try {            
            String name = tfName.getText();
            int age = Integer.parseInt(tfAge.getText());
            double weight =Double.parseDouble(tfWeight.getText()); // ex
            person.setName(name); // ex
            person.setAge(age); // ex
            person.setWeightKg(weight); // ex
        } catch (IllegalArgumentException ex) {
            JOptionPane.showMessageDialog(this,
                    ex.getMessage(),
                    "Input error",
                    JOptionPane.ERROR_MESSAGE);
        } catch (ValueInvalidException ex) {
           JOptionPane.showMessageDialog(this,
                    "Date of birth must be in yyyy/mm/dd format",
                    "Input error",
                    JOptionPane.ERROR_MESSAGE);
        }        // TODO add your handling code here:
    }//GEN-LAST:event_btUpdateActionPerformed

    private void lstPersonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstPersonMouseClicked

 /*lstPerson.addMouseListener(new MouseAdapter() {
        private JWindow window = null;

        @Override public void mouseClicked(MouseEvent e) {
            if(window != null) {
                window.dispose();
                window = null ;
            }
            if (e.getButton() == 3) { // e.isPopupTrigger() is not working on my Mac
                String options[] = {"Delete"};
                JList<String>list = new JList<String>(options);
                list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
                window = new JWindow();
                window.getContentPane().add(new JScrollPane(list));
                window.pack();
                window.setLocation(e.getXOnScreen(), e.getYOnScreen());
                window.setVisible(true);
            }
        }
    });*/
 if(SwingUtilities.isRightMouseButton(evt)){
     lstPerson.setSelectedIndex(lstPerson.locationToIndex(evt.getPoint()));
    popupMenue.show(lstPerson, evt.getX(), evt.getY());
 }

    }//GEN-LAST:event_lstPersonMouseClicked

    private void jScrollPane1MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jScrollPane1MouseReleased
       int selected = lstPerson.getSelectedIndex();
       if (selected == -1) {
            return; // should never happen
        }
        modelPersonList.remove(selected);
    }//GEN-LAST:event_jScrollPane1MouseReleased

    private void DeleteSelectedMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_DeleteSelectedMouseReleased
       int selected = lstPerson.getSelectedIndex();
       if (selected == -1) {
            return; // should never happen
        }
        modelPersonList.remove(selected);
    }//GEN-LAST:event_DeleteSelectedMouseReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Day05PeopleSorted.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Day05PeopleSorted.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Day05PeopleSorted.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Day05PeopleSorted.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Day05PeopleSorted().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem DeleteSelected;
    private javax.swing.JButton btAdd;
    private javax.swing.JButton btByAge;
    private javax.swing.JButton btByName;
    private javax.swing.JButton btByWeight;
    private javax.swing.JButton btDelete;
    private javax.swing.JButton btUpdate;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblErrorAge;
    private javax.swing.JLabel lblErrorName;
    private javax.swing.JLabel lblErrorWeight;
    private javax.swing.JList<Person> lstPerson;
    private javax.swing.JPopupMenu popupMenue;
    private javax.swing.JTextField tfAge;
    private javax.swing.JTextField tfName;
    private javax.swing.JTextField tfWeight;
    // End of variables declaration//GEN-END:variables
}
