package day05peoplesorted;

import java.text.ParseException;
import java.util.Comparator;

class ValueInvalidException extends Exception {
    public ValueInvalidException(String msg) {
        super(msg);
    }
}

public class Person {

    public Person(String name, int age, double weightKg) throws ValueInvalidException {
        setName(name);
        setAge(age);
        setWeightKg(weightKg);
    }
    public Person(String dataLine) throws ValueInvalidException {
        try {
            String[] data = dataLine.split(";");
            if (data.length != 3) {
                throw new IllegalArgumentException("Invalid number of items in data line");
            }
            setName(data[0]); // ex
            setAge(Integer.parseInt(data[1])); // ex NumberFormatException
           setWeightKg(Double.parseDouble(data[2])); // ex ParseException
        } catch (NumberFormatException  ex) {
            throw new IllegalArgumentException("Parsing error");
        }
    }
    
//Overridden toString method
     @Override 
    public String toString() {
        return String.format("%s is %d  years old weighted  %.2f",
                name, age, weightKg);
    }
     String toDataString() {
        return String.format("%s;%d;%.2f", name, age, weightKg);
    }
    private String name;
    private int age;
    private double weightKg;
  
    
    
    public String getName() {
        return name;
    }

    public void setName(String name) throws ValueInvalidException {
        if (!isNameValid(name)) {
            throw new ValueInvalidException("Name invalid");
        }
        this.name = name;
    }

    public static boolean isNameValid(String nameToTest) {
        if (nameToTest.length() < 1 || nameToTest.length() > 50||!nameToTest.matches("[A-Za-z.\\s_.-]+")) {
            return false;
        }
        // TODO: add other requirements
        return true;
    }
    
    public int getAge() {
        return age;
    }

    public void setAge(int age) { // FIXME:
        this.age = age;
    }

    public double getWeightKg() {
        return weightKg;
    }

    public void setWeightKg(double weightKg) { // FIXME:
        this.weightKg = weightKg;
    }
    

        // anonymous class used here as comparator instance
    final static Comparator<Person> comparatorByName = new Comparator<Person>() {
        @Override
        public int compare(Person p1, Person p2) {
            return p1.getName().compareTo(p2.getName());
        }
    };
     final static Comparator<Person> comparatorByNameDesc = new Comparator<Person>() {
        @Override
        public int compare(Person p1, Person p2) {
            return p2.getName().compareTo(p1.getName());
        }
    };
//
    final static Comparator<Person> comparatorByAge = new Comparator<Person>() {
        @Override
        public int compare(Person p1, Person p2) {
            return p1.getAge() - p2.getAge();
        }
    };
     final static Comparator<Person> comparatorByAgeDesc = new Comparator<Person>() {
        @Override
        public int compare(Person p1, Person p2) {
            return p2.getAge() - p1.getAge();
        }
    };
    //
    final static Comparator<Person> comparatorByWeight = new Comparator<Person>() {
        @Override
        public int compare(Person p1, Person p2) {
            if (p1.getWeightKg() == p2.getWeightKg()) {
                return 0;
            }
            if (p1.getWeightKg() > p2.getWeightKg()) {
                return 1;
            } else {
                return -1;
            }
        }

    };
    final static Comparator<Person> comparatorByWeightDesc = new Comparator<Person>() {
        @Override
        public int compare(Person p1, Person p2) {
            if (p2.getWeightKg() == p1.getWeightKg()) {
                return 0;
            }
            if (p2.getWeightKg() > p1.getWeightKg()) {
                return 1;
            } else {
                return -1;
            }
        }

    };


    
}
